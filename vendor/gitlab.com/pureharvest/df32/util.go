package df32

import (
	"bufio"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"io"

	"github.com/pkg/errors"
)

type configItem struct {
	Name    string
	Value   string
	LineNum int
}

// readConfig will read a config line and return the key and value.
// Both default to blank if not present
func readConfigLine(line string) (key string, value string) {
	parts := strings.SplitN(line, " ", 2)

	key = ""
	value = ""

	if len(parts) > 0 {
		key = strings.ToUpper(strings.TrimSpace(parts[0]))

	}

	if len(parts) > 1 {
		value = strings.TrimSpace(parts[1])
	}

	return key, value
}

func readIntermediate(r io.Reader) ([]configItem, error) {
	var items []configItem
	scanner := bufio.NewScanner(r)

	lineNum := 0
	for scanner.Scan() {
		line := scanner.Text()
		lineNum++
		// Ignore comments and empty lines
		if strings.HasPrefix(line, ";") || strings.TrimSpace(line) == "" {
			continue
		}

		key, value := readConfigLine(line)

		if value == "" {
			return items, errors.Errorf("config error at line %d.", lineNum)
		}

		item := configItem{
			Name:    key,
			Value:   value,
			LineNum: lineNum,
		}

		items = append(items, item)
	}

	return items, nil
}

func readIntFile(filename string) ([]configItem, error) {
	file, err := os.Open(filename)

	if err != nil {
		return nil, err
	}
	defer file.Close()

	return readIntermediate(file)
}

func parseConnectionID(s string) (dbConnection, error) {
	id := dbConnection{}

	parts := strings.Split(s, ",")

	if len(parts) != 3 {
		return id, errors.New("invalid connection id. Not enough parts")
	}
	silent, err := strconv.ParseBool(strings.TrimSpace(parts[2]))

	if err != nil {
		return id, errors.New("invalid connection id: " + parts[2])
	}

	id.Name = strings.TrimSpace(parts[0])
	id.ConnectionString = strings.TrimSpace(parts[1])
	id.Silent = silent

	return id, nil
}

// searchPath returns the first absolute path of file if it exists in the
// search paths, otherwise it returns an empty string
func searchPath(file string, paths []string) string {
	for _, p := range paths {
		path := filepath.Join(filepath.Clean(p), file)
		if _, err := os.Stat(path); !os.IsNotExist(err) {
			return path
		}
	}

	return ""
}
