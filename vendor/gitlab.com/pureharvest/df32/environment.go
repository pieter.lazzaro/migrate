package df32

import (
	"os"
	"path"
	"path/filepath"

	"io"

	"github.com/pkg/errors"
)

type dbConnection struct {
	Name             string
	ConnectionString string
	Silent           bool
}

type Environment struct {
	paths []string

	Driver *MSSQLDRV

	filelistFile *os.File
	Filelist     *Filelist
}

func NewEnvironment(paths []string) (*Environment, error) {
	var err error

	env := Environment{
		paths: paths,
	}

	if env.filelistFile, err = env.OpenOrCreate("filelist.cfg"); err != nil {
		return nil, errors.Wrap(err, "could not load filelist")
	}

	if env.Filelist, err = NewFilelistFromReader(env.filelistFile); err != nil {
		return nil, errors.Wrap(err, "could not read filelist")
	}

	drv, err := NewMSSQLDRV(paths)

	if err != nil {
		return nil, errors.Wrap(err, "could not load mssqldrv")
	}

	env.Driver = drv

	return &env, nil
}

func (env *Environment) OpenOrCreate(name string) (*os.File, error) {

	filePath := searchPath(name, env.paths)
	if filePath == "" {

		return os.Create(path.Join(env.paths[0], name))

	}

	return os.OpenFile(filePath, os.O_RDWR, 0666)

}

func (env *Environment) WriteFileDefinition(table Table) error {

	path := searchPath(table.Name+".fd", env.paths)

	if path == "" {
		path = filepath.Join(env.paths[0], table.Name+".fd")
	}

	file, err := os.Create(path)

	if err != nil {
		return errors.Wrap(err, "could not write file definition")
	}

	defer file.Close()

	return errors.Wrap(table.FileDefinition(file), "could not write file definition")
}

func (env *Environment) DFPATH() []string {
	return env.paths
}

func (env *Environment) WriteFilelist() error {
	if env.filelistFile != nil {
		env.filelistFile.Seek(0, io.SeekStart)
		return env.Filelist.WriteTo(env.filelistFile)
	}

	return errors.New("filelist not open")
}

func (env *Environment) Close() error {

	if env.filelistFile != nil {
		env.filelistFile.Close()
	}

	return nil
}
