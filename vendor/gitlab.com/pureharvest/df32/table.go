package df32

import (
	"fmt"
	"io"
	"strings"
	"text/template"

	"github.com/pkg/errors"
)

type Table struct {
	Number      int64 `json:"file_num"`
	Name        string
	DisplayName string `json:"display_name"`
	IntermediateFile
	Fields  []Field
	Indexes []Index
}

func writeIntermediateFile(t IntermediateFile, wr io.Writer) error {

	funcMap := template.FuncMap{
		"upper": strings.ToUpper,
	}

	temp := template.Must(template.New("int").Funcs(funcMap).Parse(intTemplate))

	return temp.Execute(wr, t)

}

func (t Table) FileDefinition(wr io.Writer) error {
	if _, err := fmt.Fprintf(wr, "#REPLACE FILE%d %s\n", t.Number, t.Name); err != nil {
		return errors.Wrap(err, "could not write file definition")
	}

	if _, err := fmt.Fprintf(wr, "#REPLACE %s.RECNUM |FN%d,0\n", t.Name, t.Number); err != nil {
		return errors.Wrap(err, "could not write file definition")
	}
	for i, field := range t.Fields {
		ft, err := ConvertSQLTypeToDataflex(field.Type)

		if err != nil {
			return errors.Wrap(err, "could not write file definition")
		}

		ft = convertToFileDefinitionType(ft)

		if _, err := fmt.Fprintf(wr, "#REPLACE %s.%s |F%s%d,%d\n", t.Name, field.Name, ft, t.Number, i+1); err != nil {
			return errors.Wrap(err, "could not write file definition")
		}
	}
	return nil
}

func convertToFileDefinitionType(t string) string {
	switch t {
	case "ascii":
		return "S"
	case "text":
		return "S"
	case "number":
		return "N"
	case "date":
		return "D"
	case "datetime":
		return "D"
	}

	return ""
}
