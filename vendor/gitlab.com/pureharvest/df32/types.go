package df32

import (
	"errors"
	"strings"
)

const (
	DefaultASCIIType    = "NVARCHAR"
	DefaultNumberType   = "NUMERIC"
	DefaultDateType     = "DATE"
	DefaultTextType     = "NTEXT"
	DefaultBinaryType   = "BINARY"
	DefaultDatetimeType = "DATETIME2"
)

type TypeMap struct {
	ascii    string
	number   string
	date     string
	text     string
	binary   string
	datetime string
}

func (m TypeMap) ToSQL(t string) string {

	switch strings.ToLower(t) {
	case "ascii":
		if m.ascii == "" {
			return DefaultASCIIType
		}
		return strings.ToUpper(m.ascii)
	case "number":
		if m.number == "" {
			return DefaultNumberType
		}
		return strings.ToUpper(m.number)
	case "date":
		if m.date == "" {
			return DefaultDateType
		}
		return strings.ToUpper(m.date)
	case "text":
		if m.text == "" {
			return DefaultTextType
		}
		return strings.ToUpper(m.text)
	case "binary":
		if m.binary == "" {
			return DefaultBinaryType
		}
		return strings.ToUpper(m.binary)
	case "datetime":
		if m.datetime == "" {
			return DefaultDatetimeType
		}
		return strings.ToUpper(m.datetime)
	default:
		return strings.ToUpper(t)
	}

}

// ConvertSQLTypeToDataflex will take a SQL datatype and return the equivilent Dataflex
// type or an error if it can't be converted
func ConvertSQLTypeToDataflex(t string) (string, error) {
	t = strings.ToLower(t)
	switch t {
	case "ascii":
		return t, nil
	case "number":
		return t, nil
	case "binary":
		return t, nil
	case "char":
		fallthrough
	case "nchar":
		fallthrough
	case "varchar":
		fallthrough
	case "nvarchar":
		return "ascii", nil
	case "decimal":
		fallthrough
	case "int":
		fallthrough
	case "smallint":
		fallthrough
	case "numeric":
		return "number", nil
	case "text":
		fallthrough
	case "ntext":
		return "text", nil
	case "date":
		fallthrough
	case "datetime":
		fallthrough
	case "datetime2":
		return "date", nil
	}

	return "", errors.New("unsupportred type")
}

var DefaultDefaults = map[string]string{
	"ascii":    "''",
	"char":     "''",
	"datetime": "'0001-01-01'",
	"date":     "'0001-01-01'",
	"number":   "0",
	"text":     "''",
}
