package df32

import (
	"bytes"
	"os"
	"testing"

	"github.com/pkg/errors"

	"github.com/stretchr/testify/assert"
)

func Test_ReadFilelist(t *testing.T) {

	file, err := os.Open("./test-fixtures/filelist.cfg")

	if err != nil {
		t.Fatal(err)
	}
	defer file.Close()

	filelist, err := NewFilelistFromReader(file)

	entry, err := filelist.Get(1)

	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, FilelistEntry{Number: 1, Driver: "DATAFLEX", Name: "TEST", DisplayName: "Test File", DataflexName: "TEST"}, entry)
}

func Test_FilelistFromReader(t *testing.T) {

	tests := []struct {
		name     string
		input    []byte
		filelist *Filelist
		err      error
	}{
		{
			name:  "empty reader",
			input: []byte{},
			filelist: &Filelist{
				data: make([]byte, entrySize*maxEntries),
			},
			err: nil,
		},
		{
			name:     "partial reader fails",
			input:    []byte{0, 0, 0},
			filelist: nil,
			err:      errors.New("could not read filelist"),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			buf := bytes.NewReader(test.input)

			filelist, err := NewFilelistFromReader(buf)

			if test.err != nil {
				assert.Equal(t, test.err.Error(), err.Error())
			}
			assert.Equal(t, test.filelist, filelist)

		})
	}
}

func makeByteSlice(val string, len int) []byte {
	s := make([]byte, len)

	copy(s, val)

	return s
}

func Test_WriteFilelist(t *testing.T) {
	filelist := NewFilelist()

	entry := FilelistEntry{Number: 1, Driver: "DATAFLEX", Name: "TEST", DisplayName: "Test File", DataflexName: "TEST"}
	filelist.Set(entry)

	var buf bytes.Buffer

	err := filelist.WriteTo(&buf)

	if err != nil {
		t.Fatal(err)
	}

	output := buf.Bytes()

	assert.Equal(t, makeByteSlice(entry.RootName(), rootLen), output[128:169])
	assert.Equal(t, makeByteSlice(entry.DisplayName, displayLen), output[169:202])
	assert.Equal(t, makeByteSlice(entry.DataflexName, dataflexLen), output[202:256])
}
