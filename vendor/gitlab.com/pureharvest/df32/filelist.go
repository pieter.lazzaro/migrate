package df32

import (
	"bytes"
	"fmt"
	"io"

	"io/ioutil"

	"strings"

	"github.com/pkg/errors"
)

type FilelistEntry struct {
	Number       int64
	Driver       string
	Name         string
	DataflexName string
	DisplayName  string
}

func (f FilelistEntry) RootName() string {
	if f.Driver == "" || f.Driver == "DATAFLEX" {
		return f.Name
	}

	return fmt.Sprintf("%s:%s", f.Driver, f.Name)
}

func (f FilelistEntry) Validate() error {

	if len(f.RootName()) > rootLen {
		return errors.Errorf("root name must be less that %d", rootLen)
	}

	if len(f.DisplayName) > displayLen {
		return errors.Errorf("display name must be less that %d", displayLen)
	}

	if len(f.DataflexName) > dataflexLen {
		return errors.Errorf("dataflex name must be less that %d", dataflexLen)
	}

	return nil
}

func (entry FilelistEntry) Bytes() []byte {

	if entry.DataflexName == "" {
		entry.DataflexName = entry.Name
	}
	return []byte(fmt.Sprintf(
		"%s%s%s%s%s%s",
		entry.RootName(), strings.Repeat("\x00", rootLen-len(entry.RootName())),
		entry.DisplayName, strings.Repeat("\x00", displayLen-len(entry.DisplayName)),
		entry.DataflexName, strings.Repeat("\x00", dataflexLen-len(entry.DataflexName)),
	))
}

const (
	maxEntries  = 4096
	entrySize   = 128
	rootLen     = 41
	displayLen  = 33
	dataflexLen = 54
)

func NewFilelist() *Filelist {
	return &Filelist{
		data: make([]byte, maxEntries*entrySize),
	}
}

func NewFilelistFromReader(r io.Reader) (*Filelist, error) {

	filelist := NewFilelist()

	data, err := ioutil.ReadAll(r)

	if err != nil {
		return nil, errors.Wrap(err, "could not read filelist")
	}

	if len(data)%entrySize != 0 {
		return nil, errors.New("could not read filelist")
	}

	copy(filelist.data, data)

	return filelist, nil
}

type Filelist struct {
	data []byte
}

func (f *Filelist) NextAvailable() int {
	entry := 1

	for entry < maxEntries {
		if f.data[entry*entrySize] == 0 {
			return entry
		}
	}

	return 0
}

// Get retrieves the entry at i
func (f *Filelist) Get(i int) (FilelistEntry, error) {

	if i > maxEntries {
		return FilelistEntry{}, errors.Errorf("filelist only supports %d entries", maxEntries)
	}

	entry := FilelistEntry{
		Number: int64(i),
		Driver: "DATAFLEX",
	}

	offset := i * entrySize
	displayOffset := offset + rootLen
	dataflexOffset := displayOffset + displayLen

	rootName := string(bytes.Trim(f.data[offset:offset+rootLen], "\x00"))
	entry.DisplayName = string(bytes.Trim(f.data[displayOffset:displayOffset+displayLen], "\x00"))
	entry.DataflexName = string(bytes.Trim(f.data[dataflexOffset:dataflexOffset+dataflexLen], "\x00"))

	s := strings.Split(rootName, ":")

	if len(s) == 2 {
		entry.Driver = s[0]
		entry.Name = s[1]
	} else {
		entry.Name = s[0]
	}

	return entry, nil
}

func (f *Filelist) GetAll() []FilelistEntry {
	var entries []FilelistEntry

	for i := 1; i < maxEntries; i++ {
		entry, err := f.Get(i)

		if err != nil {
			continue
		}

		if entry.Name != "" {
			entries = append(entries, entry)
		}
	}

	return entries
}

func (f *Filelist) GetByName(name string) (FilelistEntry, error) {
	for i := 1; i < maxEntries; i++ {
		entry, err := f.Get(i)

		if err != nil {
			return FilelistEntry{}, err
		}

		if entry.Name == name {
			return entry, nil
		}
	}

	return FilelistEntry{}, errors.New("filelist entry not found")

}

func (f *Filelist) Set(entry FilelistEntry) error {

	if entry.Number >= maxEntries {
		return errors.Errorf("filelist only supports %d entries", maxEntries)
	}

	if err := entry.Validate(); err != nil {
		return errors.Wrap(err, "invalid filelist entry")
	}

	offset := entry.Number * entrySize
	copy(f.data[offset:offset+entrySize], entry.Bytes())

	return nil
}

func (filelist *Filelist) WriteTo(wr io.Writer) error {

	n, err := wr.Write(filelist.data)

	if err != nil {
		return errors.Wrap(err, "could not write filelist")
	}
	if n != len(filelist.data) {
		return errors.New("could not write filelist")
	}

	return nil
}
