package df32

type Driver interface {
	LoadTable(entry FilelistEntry) (Table, error)
	SaveTable(table Table) error
}
