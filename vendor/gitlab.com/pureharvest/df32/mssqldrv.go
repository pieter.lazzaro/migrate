package df32

import (
	"database/sql"
	"fmt"
	"io"
	"path/filepath"
	"strconv"
	"strings"

	"os"

	"regexp"

	_ "github.com/denisenkom/go-mssqldb"
	"github.com/pkg/errors"
)

const intTemplate = `DRIVER_NAME MSSQLDRV
SERVER_NAME {{.Server}}
DATABASE_NAME {{.DatabaseName}}
SCHEMA_NAME {{.Schema}}

{{ if .System }}SYSTEM_FILE YES
{{- else -}}
{{ if .Recnum }}RECNUM_TABLE YES{{ end }}
PRIMARY_INDEX {{.PrimaryIndexNumber}}
GENERATE_RECORD_ID_METHOD {{ if .RecordIdentity }}{{ .RecordIdentity }}{{ else }}IDENTITY_COLUMN{{end}}
{{- end }}
TABLE_CHARACTER_FORMAT {{ if .Format }}{{ .Format }}{{ else }}ANSI{{end}}
{{ if .UseDummyZeroDate }}USE_DUMMY_ZERO_DATE YES{{ end }}
{{- range $i, $field := .Fields -}}
{{- if .NeedsIntermediate }}

FIELD_NUMBER {{ .Number }}
{{- if .NeedsIntermediateLength }}
FIELD_LENGTH {{ .Size.Length }}
{{- end -}}
{{- if .Index }}
FIELD_INDEX {{ .Index }}
{{- end -}}
{{- if .RelatesTo.File }}
FIELD_RELATED_FILE {{.RelatesTo.File}}
FIELD_RELATED_FIELD {{.RelatesTo.Field}}
{{- end -}}
{{- end -}}
{{- end -}}
{{ "" }}
{{- range $i, $index := .Indexes }}

INDEX_NUMBER {{ .Number }}
INDEX_NAME {{ .Name }}
{{- end }}
`

type IntermediateFile struct {
	DatabaseName       string `json:"database_name"`
	Server             string
	Schema             string
	Recnum             bool
	System             bool
	PrimaryIndexNumber int
	RecordIdentity     string
	Format             string
	UseDummyZeroDate   bool
	Fields             []Field
	Indexes            []Index
}

type MSSQLDRV struct {
	paths    []string
	types    TypeMap
	defaults map[string]string

	db          *sql.DB
	connections []dbConnection
}

func NewMSSQLDRV(paths []string) (*MSSQLDRV, error) {
	drv := MSSQLDRV{
		paths:    paths,
		defaults: DefaultDefaults,
	}

	configFile := searchPath("mssqldrv.int", paths)

	if configFile == "" {
		return &drv, nil
	}

	config, err := readIntFile(configFile)

	if err != nil {
		return nil, errors.Wrap(err, "error loading driver config")
	}

	for _, item := range config {
		switch item.Name {
		case "DFCONNECTIONID":
			id, err := parseConnectionID(item.Value)

			if err != nil {
				return nil, errors.Wrapf(err, "config error at line %d.", item.LineNum)
			}

			drv.connections = append(drv.connections, id)

		case "MAP_DFDATETIME_TO_SQLTYPE":
			drv.types.datetime = strings.ToLower(item.Value)
		case "MAP_DFDATE_TO_SQLTYPE":
			drv.types.date = strings.ToLower(item.Value)
		case "MAP_DFASCII_TO_SQLTYPE":
			drv.types.ascii = strings.ToLower(item.Value)
		case "MAP_DFTEXT_TO_SQLTYPE":
			drv.types.text = strings.ToLower(item.Value)
		case "DEFAULT_DEFAULT_ASCII":
			drv.defaults["ascii"] = strings.ToLower(item.Value)
		case "DEFAULT_DEFAULT_NUMERIC":
			drv.defaults["number"] = strings.ToLower(item.Value)
		case "DEFAULT_DEFAULT_DATE":
			drv.defaults["date"] = strings.ToLower(item.Value)
		case "DEFAULT_DEFAULT_TEXT":
			drv.defaults["text"] = strings.ToLower(item.Value)
		case "DEFAULT_DEFAULT_BINARY":
			drv.defaults["binary"] = strings.ToLower(item.Value)
		case "DEFAULT_DEFAULT_DATETIME":
			drv.defaults["datetime"] = strings.ToLower(item.Value)
		}

	}
	return &drv, nil
}

func (drv *MSSQLDRV) SetDB(db *sql.DB) {
	drv.db = db
}

func (drv *MSSQLDRV) Connections() []string {
	var connections []string

	for _, c := range drv.connections {
		connections = append(connections, ConvertDataflexConnectionString(c.ConnectionString))
	}

	return connections
}

func (drv *MSSQLDRV) Generator() SQLGenerator {

	if drv == nil {
		return DefaultGenerator
	}

	return SQLGenerator{
		types:    drv.types,
		defaults: drv.defaults,
	}
}

func (drv *MSSQLDRV) findAndLoadIntermediateFile(name string) (IntermediateFile, error) {

	intFileName := searchPath(name+".int", drv.paths)

	if intFileName == "" {
		return IntermediateFile{}, errors.New("could not find intermediate file")
	}

	intFile, err := os.Open(intFileName)

	if err != nil {
		return IntermediateFile{}, errors.Wrap(err, "could not open intermediate file")
	}

	defer intFile.Close()

	return loadIntermediateFile(intFile)

}

func (drv *MSSQLDRV) LoadTableByName(name string) (Table, error) {

	table := Table{
		Name: name,
	}

	intTable, err := drv.findAndLoadIntermediateFile(name)

	if err != nil {
		return table, errors.Wrap(err, "could not load table")
	}

	table.IntermediateFile = intTable

	if drv.db != nil {
		db, err := drv.Connect(table.Server)

		if err != nil {
			return table, errors.Wrap(err, "could not connect to database for table")
		}
		drv.db = db
	}

	if err := drv.LoadTableFromDB(&table, drv.db); err != nil {
		return table, errors.Wrap(err, "could not load table structure")
	}

	return table, nil
}

func (drv *MSSQLDRV) LoadTableByNameWithDB(name string, db *sql.DB) (Table, error) {

	table := Table{}
	intTable, err := drv.findAndLoadIntermediateFile(name)

	if err != nil {
		return table, errors.Wrap(err, "could not load table")
	}

	table.IntermediateFile = intTable

	if err := drv.LoadTableFromDB(&table, db); err != nil {
		return table, errors.Wrap(err, "could not load table structure")
	}

	return table, nil
}

func (drv *MSSQLDRV) LoadTable(entry FilelistEntry) (Table, error) {

	if !strings.EqualFold(entry.Driver, "MSSQLDRV") {
		return Table{}, errors.New("table is not a MSSQL table")
	}

	table, err := drv.LoadTableByName(entry.Name)

	if err != nil {
		return table, err
	}

	table.DisplayName = entry.DisplayName
	table.Number = entry.Number

	return table, nil
}

func (drv *MSSQLDRV) LoadTableWithDB(entry FilelistEntry, db *sql.DB) (Table, error) {

	if !strings.EqualFold(entry.Driver, "MSSQLDRV") {
		return Table{}, errors.New("table is not a MSSQL table")
	}

	table, err := drv.LoadTableByNameWithDB(entry.Name, db)

	if err != nil {
		return table, err
	}

	table.DisplayName = entry.DisplayName
	table.Number = entry.Number

	return table, nil
}

func loadIntermediateFile(r io.Reader) (IntermediateFile, error) {
	intTable := IntermediateFile{}

	config, err := readIntermediate(r)

	if err != nil {
		return intTable, errors.Wrap(err, "can't load table int file")
	}

	// Use pointer to tell if we have got a def
	var currentField *Field
	var currentIndex *Index

	for _, item := range config {
		switch strings.ToUpper(item.Name) {
		case "SERVER_NAME":
			intTable.Server = item.Value
		case "DATABASE_NAME":
			intTable.DatabaseName = item.Value
		case "SCHEMA_NAME":
			intTable.Schema = item.Value
		case "RECNUM_TABLE":
			if strings.ToUpper(item.Value) == "YES" {
				intTable.Recnum = true
			}
		case "SYSTEM_FILE":
			if strings.ToUpper(item.Value) == "YES" {
				intTable.System = true
			}
		case "PRIMARY_INDEX":
			index, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "primary index must be a number")
			}
			intTable.PrimaryIndexNumber = index
		case "GENERATE_RECORD_ID_METHOD":
			intTable.RecordIdentity = item.Value
		case "TABLE_CHARACTER_FORMAT":
			intTable.Format = item.Value
		case "USE_DUMMY_ZERO_DATE":
			if strings.ToUpper(item.Value) == "YES" {
				intTable.UseDummyZeroDate = true
			}
		case "FIELD_NUMBER":
			if currentIndex != nil {
				// TODO: Validate index config
				intTable.Indexes = append(intTable.Indexes, *currentIndex)
				currentIndex = nil
			}
			fieldNum, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field number must be a number")
			}

			if currentField == nil {
				currentField = &Field{
					Number: fieldNum,
				}
			}

			if currentField.Number != fieldNum {
				// TODO: Validate field config
				intTable.Fields = append(intTable.Fields, *currentField)
				currentField = &Field{
					Number: fieldNum,
				}
			}
		case "FIELD_INDEX":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.LineNum)
			}
			index, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field index must be a number")
			}

			currentField.Index = index
		case "FIELD_LENGTH":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.LineNum)
			}
			length, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field index must be a number")
			}

			currentField.Size.Length = length
		case "FIELD_PRECISION":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.LineNum)
			}
			precision, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field index must be a number")
			}

			currentField.Size.Precision = precision
		case "FIELD_RELATED_FILE":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.LineNum)
			}
			fileNum, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field related file must be a number")
			}

			currentField.RelatesTo.File = fileNum
		case "FIELD_RELATED_FIELD":
			if currentField == nil {
				return intTable, errors.Errorf("field properties must come after field numbers. (%d)", item.LineNum)
			}
			fieldNum, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "field related field must be a number")
			}

			currentField.RelatesTo.Field = fieldNum
		case "INDEX_NUMBER":
			if currentField != nil {
				// TODO: Validate field config
				intTable.Fields = append(intTable.Fields, *currentField)
				currentField = nil
			}
			index, err := strconv.Atoi(item.Value)
			if err != nil {
				return intTable, errors.Wrap(err, "index number must be a number")
			}

			if currentIndex == nil {
				currentIndex = &Index{
					Number: index,
				}
			}

			if currentIndex.Number != index {
				// TODO: Validate index config
				intTable.Indexes = append(intTable.Indexes, *currentIndex)
				currentIndex = &Index{
					Number: index,
				}
			}

		case "INDEX_NAME":
			if currentIndex == nil {
				return intTable, errors.Errorf("index properties must come after field numbers. (%d)", item.LineNum)
			}

			currentIndex.Name = item.Value
		}
	}

	if currentField != nil {
		// TODO: Validate field config
		intTable.Fields = append(intTable.Fields, *currentField)
		currentField = nil
	}

	if currentIndex != nil {
		// TODO: Validate field config
		intTable.Indexes = append(intTable.Indexes, *currentIndex)
		currentIndex = nil
	}

	return intTable, nil
}

func (drv *MSSQLDRV) LoadTableFromDB(table *Table, db *sql.DB) error {
	dbName := getDatabaseName(drv.connections[0].ConnectionString)

	rows, err := db.Query(`
	SELECT ORDINAL_POSITION, COLUMN_NAME, DATA_TYPE, COLUMN_DEFAULT, NUMERIC_PRECISION, NUMERIC_SCALE, CHARACTER_MAXIMUM_LENGTH
FROM [`+dbName+`].INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = ? AND TABLE_SCHEMA=?`, table.IntermediateFile.DatabaseName, table.IntermediateFile.Schema)

	if err != nil {
		return errors.Wrapf(err, "could not load table information: %#v", table.IntermediateFile)
	}

	var fields []Field

	for rows.Next() {
		var size FieldSize
		var name, dataType, defaultValue sql.NullString
		var number int
		var precision, scale, maxLen sql.NullInt64
		if err := rows.Scan(&number, &name, &dataType, &defaultValue, &precision, &scale, &maxLen); err != nil {
			return errors.Wrap(err, "could not load table information")
		}

		if strings.EqualFold(name.String, "recnum") {
			continue
		}
		switch dataType.String {
		case "smallint":
			size.Length = 4
		case "int":
			size.Length = 6
		case "nchar":
			fallthrough
		case "char":
			fallthrough
		case "varchar":
			fallthrough
		case "nvarchar":
			size.Length = int(maxLen.Int64)
		case "decimal":
			fallthrough
		case "numeric":
			size.Length = int(precision.Int64)
			size.Precision = int(scale.Int64)
		}

		ft, err := ConvertSQLTypeToDataflex(dataType.String)

		if err != nil {
			return errors.Wrapf(err, "unknown type in %s", dataType.String)
		}

		field := Field{
			Number: number - 1,
			Name:   name.String,
			Type:   ft,
			Size:   FieldSize(size),
		}

		fields = append(fields, field)
	}

	for _, field := range table.IntermediateFile.Fields {
		if field.Number > len(fields) {
			return errors.Errorf("intermediate file refers to a field %d that doesn't exist", field.Number)
		}

		if field.Index != 0 {
			fields[field.Number-1].Index = field.Index
		}

		if field.RelatesTo.File != 0 {
			fields[field.Number-1].RelatesTo = field.RelatesTo
		}

		if field.Size.SQLLength() != 0 {
			fields[field.Number-1].Size = field.Size
		}
	}

	table.Fields = fields

	rows, err = db.Query(`
SELECT  ind.name AS IndexName
      , ind.is_primary_key AS IsPrimaryKey
      , ind.is_unique AS IsUniqueIndex
      , col.name AS ColumnName
      , ic.is_included_column AS IsIncludedColumn
      , ic.key_ordinal AS ColumnOrder
	  , ic.is_descending_key As IsDescending
FROM    sys.indexes ind
        INNER JOIN sys.index_columns ic
            ON ind.object_id = ic.object_id
               AND ind.index_id = ic.index_id
        INNER JOIN sys.columns col
            ON ic.object_id = col.object_id
               AND ic.column_id = col.column_id
        INNER JOIN sys.tables t
            ON ind.object_id = t.object_id
WHERE   t.is_ms_shipped = 0
	AND OBJECT_SCHEMA_NAME(ind.object_id) = ? AND OBJECT_NAME(ind.object_id) = ?
ORDER BY ind.is_primary_key DESC
      , ind.is_unique DESC
      , ind.name --IndexName
      , ic.key_ordinal
`, table.IntermediateFile.Schema, table.IntermediateFile.DatabaseName)

	if err != nil {
		return errors.Wrap(err, "could not read indexes")
	}

	indexes := map[string]Index{}

	for rows.Next() {
		var indexName, columnName string
		var isIncluded, isDescending, isPrimary, isUnique bool
		var columnNumber int

		if err := rows.Scan(&indexName, &isPrimary, &isUnique, &columnName, &isIncluded, &columnNumber, &isDescending); err != nil {
			return errors.Wrap(err, "could not load table information")
		}

		currentIndex, found := indexes[indexName]

		if !found {
			currentIndex.Name = indexName
			currentIndex.Clustered = isPrimary
		}

		if !isIncluded {
			currentIndex.Fields = append(currentIndex.Fields, IndexField{
				Name:       columnName,
				Number:     columnNumber,
				Descending: isDescending,
			})
		}

		indexes[indexName] = currentIndex

	}

	if rows.Err() != nil {
		return errors.Wrap(rows.Err(), "could not read indexes")
	}

	for _, index := range table.IntermediateFile.Indexes {
		if _, found := indexes[index.Name]; found {

			index.Clustered = indexes[index.Name].Clustered
			index.Fields = indexes[index.Name].Fields

			table.Indexes = append(table.Indexes, index)
		}
	}

	return nil
}

// loadTableStructure will load the database table structure from MSSQL into
// the passed in table.
func (drv *MSSQLDRV) loadTableStructure(table *Table) error {

	connectionString, err := drv.connectionString(table.IntermediateFile.Server)

	if err != nil {
		return errors.Wrap(err, "could not read connection string")
	}

	db, err := sql.Open("mssql", connectionString)

	if err != nil {
		return errors.Wrap(err, "could not connect to database")
	}

	if err := db.Ping(); err != nil {
		return errors.Wrap(err, "could not connect to database")
	}

	defer db.Close()

	return drv.LoadTableFromDB(table, db)
}

func (drv *MSSQLDRV) DefaultIntermediate() IntermediateFile {
	return IntermediateFile{
		Recnum:           true,
		Format:           "ANSI",
		RecordIdentity:   "IDENTITY_COLUMN",
		UseDummyZeroDate: true,
	}
}

func (drv *MSSQLDRV) SaveTable(table Table) error {

	return drv.WriteIntermediateFile(table.Name, table.IntermediateFile)
}

func (drv *MSSQLDRV) WriteIntermediateFile(name string, table IntermediateFile) error {

	intPath := searchPath(name+".int", drv.paths)

	if intPath == "" {
		intPath = filepath.Join(drv.paths[0], name+".int")
	}

	file, err := os.Create(intPath)

	if err != nil {
		return errors.Wrap(err, "could not create intermediate file")
	}

	defer file.Close()

	return writeIntermediateFile(table, file)
}

func (drv *MSSQLDRV) DefaultConnectionString() string {
	if drv == nil {
		return ""
	}

	if len(drv.connections) > 0 {
		return "DFCONNID=" + drv.connections[0].Name
	}

	return ""

}

func (drv *MSSQLDRV) Connect(connectionString string) (*sql.DB, error) {
	// Use the first connection id defined in MSSQLDRV.int if none is specified
	if connectionString == "" && drv.DefaultConnectionString() != "" {
		connectionString = drv.DefaultConnectionString()
	}

	connectionString, err := drv.connectionString(connectionString)

	if err != nil {
		return nil, errors.Wrap(err, "could not connect to database")
	}

	if connectionString == "" {
		return nil, errors.New("no connection string specified")
	}

	return sql.Open("mssql", connectionString)
}

func (drv *MSSQLDRV) connectionString(connectionString string) (string, error) {

	if strings.HasPrefix(strings.ToUpper(connectionString), "DFCONNID=") {
		id := strings.TrimSuffix(strings.TrimPrefix(strings.ToUpper(connectionString), "DFCONNID="), ";")
		var connID dbConnection
		var found bool
		for _, conn := range drv.connections {
			if strings.EqualFold(conn.Name, id) {
				connID = conn
				found = true
			}
		}

		if !found {
			return "", errors.New("connection id not found: " + id)
		}

		connectionString = connID.ConnectionString
	}

	connectionString = ConvertDataflexConnectionString(connectionString)

	return connectionString, nil
}

var serverRegexp = regexp.MustCompile("(?i)server=([^,;]+)(?:,(\\d+))?")
var uidRegexp = regexp.MustCompile("(?i)uid=")
var pwdRegexp = regexp.MustCompile("(?i)pwd=")

//very basic database regexp. Might need to make this more rebust
var databaseRegexp = regexp.MustCompile("(?i)database=([^;]+);?")

func ConvertDataflexConnectionString(connectionString string) string {

	s := serverRegexp.FindStringSubmatch(connectionString)

	if len(s) == 3 {
		if s[2] != "" {
			connectionString = strings.Replace(connectionString, s[0], fmt.Sprintf("server=%s;port=%s", s[1], s[2]), -1)
		} else {
			connectionString = strings.Replace(connectionString, s[0], fmt.Sprintf("server=%s", s[1]), -1)
		}

	}

	connectionString = uidRegexp.ReplaceAllString(connectionString, "user id=")

	connectionString = pwdRegexp.ReplaceAllString(connectionString, "password=")

	return connectionString
}

func getDatabaseName(connectionString string) string {
	s := databaseRegexp.FindStringSubmatch(connectionString)

	if len(s) != 2 {
		return "master"
	}

	return s[1]
}
