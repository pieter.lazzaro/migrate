package df32

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_SQLLength(t *testing.T) {
	tests := []struct {
		field  Field
		length string
	}{
		{
			field:  Field{Name: "TEST_ASCII", Type: "ascii", Size: FieldSize{Length: 10}},
			length: "(10)",
		},
		{
			field:  Field{Name: "TEST", Type: "date"},
			length: "",
		},
		{
			field:  Field{Name: "TEST", Type: "number", Size: FieldSize{Length: 10, Precision: 2}},
			length: "(12,2)",
		},
		{
			field:  Field{Name: "TEST", Type: "number", Size: FieldSize{Length: 10}},
			length: "(10)",
		},
	}

	for _, test := range tests {
		t.Run(test.field.Name, func(t *testing.T) {

			assert.Equal(t, test.length, sqlLength(test.field))
		})
	}
}

func Test_SQLGeneratorCreateIndex(t *testing.T) {
	tests := []struct {
		tableName string
		schema    string
		index     Index
		expected  string
	}{
		{
			tableName: "TEST",
			schema:    "test",
			index:     Index{Name: "TEST01", Fields: []IndexField{{Name: "FIELD1", Descending: true}}},
			expected:  "CREATE UNIQUE NONCLUSTERED INDEX [TEST01] ON [test].[TEST] ([FIELD1] DESC)",
		},
		{
			tableName: "TEST",
			schema:    "test",
			index:     Index{Name: "TEST01", Clustered: true, Fields: []IndexField{{Name: "FIELD1", Descending: true}}},
			expected:  "CREATE UNIQUE CLUSTERED INDEX [TEST01] ON [test].[TEST] ([FIELD1] DESC)",
		},
	}

	for _, test := range tests {
		t.Run(test.index.Name, func(t *testing.T) {
			g := SQLGenerator{}
			sql := g.CreateIndex(test.schema, test.tableName, test.index.Name, test.index.Clustered, test.index.Fields)

			assert.Equal(t, test.expected, sql)
		})
	}
}

func Test_SQLGeneratorFieldDefintion(t *testing.T) {
	generator := SQLGenerator{
		defaults: DefaultDefaults,
	}

	tests := []struct {
		table    string
		field    Field
		expected string
	}{
		{
			table:    "TEST",
			field:    Field{Name: "TEST", Type: "ascii", Size: FieldSize{Length: 10}},
			expected: "[TEST] NVARCHAR(10) CONSTRAINT [DF__TEST__TEST] DEFAULT ('') NOT NULL",
		},
		{
			table:    "TEST",
			field:    Field{Name: "TEST", Type: "date"},
			expected: "[TEST] DATE CONSTRAINT [DF__TEST__TEST] DEFAULT ('0001-01-01') NOT NULL",
		},
		{
			table:    "TEST",
			field:    Field{Name: "TEST", Type: "number", Size: FieldSize{Length: 10}},
			expected: "[TEST] NUMERIC(10) CONSTRAINT [DF__TEST__TEST] DEFAULT (0) NOT NULL",
		},
		{
			table:    "TEST",
			field:    Field{Name: "TEST", Type: "number", Size: FieldSize{Length: 10, Precision: 4}},
			expected: "[TEST] NUMERIC(14,4) CONSTRAINT [DF__TEST__TEST] DEFAULT (0) NOT NULL",
		},
	}

	for _, test := range tests {
		t.Run(test.table+test.field.Name, func(t *testing.T) {
			sql := generator.fieldDefinition(test.table, test.field)

			assert.Equal(t, test.expected, sql)
		})
	}
}

func Test_SQLGeneratorCreateTable(t *testing.T) {
	generator := SQLGenerator{
		defaults: DefaultDefaults,
	}

	tests := []struct {
		name     string
		table    Table
		expected string
	}{
		{
			table: Table{
				Name: "TEST",
				IntermediateFile: IntermediateFile{
					Schema: "test",
				},
				Fields: []Field{
					{Name: "FIELD1", Type: "ascii", Size: FieldSize{Length: 10}},
					{Name: "FIELD2", Type: "date", Size: FieldSize{Length: 6}},
				},
			},
			expected: `CREATE TABLE [test].[TEST] (
    [FIELD1] NVARCHAR(10) CONSTRAINT [DF__TEST__FIELD1] DEFAULT ('') NOT NULL,
    [FIELD2] DATE CONSTRAINT [DF__TEST__FIELD2] DEFAULT ('0001-01-01') NOT NULL
    
)`,
		},
		{
			table: Table{
				Name: "TEST",
				IntermediateFile: IntermediateFile{
					Schema: "test",
					Recnum: true,
				},
				Fields: []Field{
					{Name: "FIELD1", Type: "ascii", Size: FieldSize{Length: 10}},
				},
			},
			expected: `CREATE TABLE [test].[TEST] (
    [RECNUM] INT IDENTITY (1, 1) NOT NULL,
    [FIELD1] NVARCHAR(10) CONSTRAINT [DF__TEST__FIELD1] DEFAULT ('') NOT NULL
    
)`,
		},
		{
			table: Table{
				Name: "TEST",
				IntermediateFile: IntermediateFile{
					Schema: "test",
				},
				Fields: []Field{
					{Name: "RECNUM", Type: "identity"},
					{Name: "FIELD1", Type: "ascii", Size: FieldSize{Length: 10}},
				},
				Indexes: []Index{
					{Name: "TEST001_PK", Clustered: true, PrimaryKey: true, Fields: []IndexField{{Name: "FIELD1", Descending: true}}},
				},
			},
			expected: `CREATE TABLE [test].[TEST] (
    [RECNUM] INT IDENTITY (1, 1) NOT NULL,
    [FIELD1] NVARCHAR(10) CONSTRAINT [DF__TEST__FIELD1] DEFAULT ('') NOT NULL
    CONSTRAINT [TEST001_PK] PRIMARY KEY CLUSTERED ([FIELD1] DESC)
)`,
		},
		{
			table: Table{
				Name: "TEST",
				IntermediateFile: IntermediateFile{
					Schema: "test",
				},
				Fields: []Field{
					{Name: "FIELD1", Type: "ascii", Size: FieldSize{Length: 10}},
				},
			},
			expected: `CREATE TABLE [test].[TEST] (
    [FIELD1] NVARCHAR(10) CONSTRAINT [DF__TEST__FIELD1] DEFAULT ('') NOT NULL
    
)`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			sql := generator.CreateTable(test.table)

			assert.Equal(t, test.expected, sql)
		})
	}
}

func Test_SQLGeneratorAlterField(t *testing.T) {
	generator := SQLGenerator{
		defaults: DefaultDefaults,
	}

	tests := []struct {
		tableName string
		schema    string
		from      Field
		to        Field
		expected  string
	}{
		{
			tableName: "TEST",
			schema:    "test",
			from:      Field{Name: "FROM"},
			to:        Field{Type: "number", Size: FieldSize{Length: 10, Precision: 4}},
			expected:  `ALTER TABLE [test].[TEST] ALTER COLUMN [FROM] NUMERIC(14,4);`,
		},
	}

	for _, test := range tests {
		t.Run(test.tableName, func(t *testing.T) {
			sql := generator.AlterField(test.schema, test.tableName, test.from, test.to)

			assert.Equal(t, test.expected, sql)
		})
	}
}

func Test_SQLGeneratorRenameField(t *testing.T) {
	generator := SQLGenerator{
		defaults: DefaultDefaults,
	}

	tests := []struct {
		tableName string
		schema    string
		from      string
		to        string
		expected  string
	}{
		{
			tableName: "TEST",
			schema:    "test",
			from:      "FROM",
			to:        "TO",
			expected:  `EXEC sp_rename @objname=N'[test].[TEST].[FROM]', @newname=N'TO', @objtype=N'COLUMN';`,
		},
	}

	for _, test := range tests {
		t.Run(test.tableName, func(t *testing.T) {
			sql := generator.RenameField(test.schema, test.tableName, test.from, test.to)

			assert.Equal(t, test.expected, sql)
		})
	}
}

func Test_SQLGeneratorAddField(t *testing.T) {
	generator := SQLGenerator{
		defaults: DefaultDefaults,
	}

	tests := []struct {
		tableName string
		schema    string
		field     Field
		expected  string
	}{
		{
			tableName: "TEST",
			schema:    "test",
			field:     Field{Name: "FIELD1", Type: "ascii", Size: FieldSize{Length: 10}},
			expected:  `ALTER TABLE [test].[TEST] ADD [FIELD1] NVARCHAR(10) CONSTRAINT [DF__TEST__FIELD1] DEFAULT ('') NOT NULL;`,
		},
	}

	for _, test := range tests {
		t.Run(test.tableName, func(t *testing.T) {
			sql := generator.AddField(test.schema, test.tableName, test.field)

			assert.Equal(t, test.expected, sql)
		})
	}
}

func Test_SQLGeneratorDropField(t *testing.T) {
	generator := SQLGenerator{
		defaults: DefaultDefaults,
	}

	tests := []struct {
		tableName string
		schema    string
		field     string
		expected  string
	}{
		{
			tableName: "TEST",
			schema:    "test",
			field:     "FIELD1",
			expected:  `ALTER TABLE [test].[TEST] DROP [FIELD1];`,
		},
	}

	for _, test := range tests {
		t.Run(test.tableName, func(t *testing.T) {
			sql := generator.DropField(test.schema, test.tableName, test.field)

			assert.Equal(t, test.expected, sql)
		})
	}
}
