package df32

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"path"
	"testing"
	"time"
)

func createDB(t *testing.T, db *sql.DB, name string) {

	t.Log("Creating Database ", name)
	if _, err := db.Exec(fmt.Sprintf("CREATE DATABASE [%s]", name)); err != nil {
		t.Fatal(err)
	}

	t.Log("Creating schema...")
	if _, err := db.Exec(fmt.Sprintf("USE [%s]; EXEC (N'CREATE SCHEMA [dataflex]'); USE [master]", name)); err != nil {
		t.Fatal(err)
	}

}

func dropDB(t *testing.T, db *sql.DB, name string) {
	t.Log("Dropping Database ", name)
	if _, err := db.Exec(fmt.Sprintf("ALTER DATABASE [%s] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;", name)); err != nil {
		t.Fatal(err)
	}

	if _, err := db.Exec(fmt.Sprintf("DROP DATABASE [%s]", name)); err != nil {
		t.Fatal(err)
	}
}

func randomDBName(prefix string) string {

	rand.Seed(time.Now().UTC().UnixNano())
	const chars = "abcdefghijklmnopqrstuvwxyz0123456789"
	result := make([]byte, 10)
	for i := 0; i < 10; i++ {
		result[i] = chars[rand.Intn(len(chars))]
	}
	return prefix + string(result)

}

func CreateTestEnvironment(t *testing.T) (env *Environment, deferFn func()) {

	host := os.Getenv("MSSQL_PORT_1433_TCP_ADDR")
	user := os.Getenv("MSSQL_USER_PWD")

	if host == "" {
		host = "127.0.0.1"
	}

	databaseName := randomDBName("df")

	connectionString := fmt.Sprintf("server=%s;%s", host, user)
	t.Log(ConvertDataflexConnectionString(connectionString))
	db, err := sql.Open("mssql", ConvertDataflexConnectionString(connectionString))

	if err != nil {
		t.Fatalf("%#v", err)
	}

	if err := db.Ping(); err != nil {
		t.Fatalf("%#v", err)
	}

	createDB(t, db, databaseName)

	tmpdir, err := ioutil.TempDir(os.TempDir(), "df")

	if err != nil {
		t.Fatal(err)
	}

	intfile := fmt.Sprintf(`
DFConnectionId POSONA, %s;database=%s, 1
	`, connectionString, databaseName)

	if err := ioutil.WriteFile(path.Join(tmpdir, "mssqldrv.int"), []byte(intfile), 0666); err != nil {
		t.Fatal(err)
	}

	deferFn = func() {
		dropDB(t, db, databaseName)
		db.Close()
		os.RemoveAll(tmpdir)
	}

	env, err = NewEnvironment([]string{tmpdir})

	if err != nil {
		deferFn()
		t.Fatal(err)
	}

	return env, deferFn

}
