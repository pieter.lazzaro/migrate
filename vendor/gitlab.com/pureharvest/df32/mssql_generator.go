package df32

import (
	"fmt"
	"strings"
)

var DefaultGenerator = SQLGenerator{
	defaults: DefaultDefaults,
}

type SQLGenerator struct {
	types    TypeMap
	defaults map[string]string
}

func sqlLength(f Field) string {
	if f.Type == "date" || f.Type == "text" || f.Type == "datetime" {
		return ""
	}

	if strings.EqualFold(f.Type, "identity") {
		return " (1, 1)"
	}

	if f.Type == "number" {
		if f.Size.Precision == 0 {
			return fmt.Sprintf("(%d)", f.Size.SQLLength())
		}

		return fmt.Sprintf("(%d,%d)", f.Size.SQLLength(), f.Size.Precision)
	}

	return fmt.Sprintf("(%s)", f.Size)
}

func fieldType(field Field, types TypeMap) string {

	if strings.EqualFold(field.Type, "identity") {
		return "INT IDENTITY"
	}

	if strings.EqualFold(field.Type, "number") && field.Size.Precision == 0 && field.Size.SQLLength() <= 4 {
		return "smallint"
	}

	if strings.EqualFold(field.Type, "number") && field.Size.Precision == 0 && field.Size.SQLLength() <= 6 {
		return "int"
	}

	return types.ToSQL(field.Type) + sqlLength(field)
}

func (g SQLGenerator) fieldDefinition(table string, field Field) string {

	fieldType := fieldType(field, g.types)

	// Special case for identity fields
	if strings.EqualFold(field.Type, "identity") {
		return fmt.Sprintf("[%s] %s (1, 1) NOT NULL", field.Name, fieldType)
	}

	return fmt.Sprintf(
		"[%s] %s CONSTRAINT [DF__%s__%s] DEFAULT (%s) NOT NULL",
		field.Name,
		fieldType,
		table,
		field.Name,
		g.defaults[field.Type],
	)
}

func indexFieldsDefinition(fields []IndexField) string {
	fieldDefs := make([]string, 0, len(fields))

	for _, field := range fields {
		direction := "ASC"

		if field.Descending {
			direction = "DESC"
		}

		fieldDefs = append(fieldDefs, fmt.Sprintf("[%s] %s", field.Name, direction))
	}

	return strings.Join(fieldDefs, ",")
}

func clusteredIndex(table string, indexes []Index) string {
	var name string
	var fields []IndexField

	for _, index := range indexes {
		if index.Clustered && index.PrimaryKey {
			name = index.Name
			fields = index.Fields
			break
		}
	}

	if name == "" {
		return ""
	}

	return fmt.Sprintf("CONSTRAINT [%s] PRIMARY KEY CLUSTERED (%s)", name, indexFieldsDefinition(fields))
}

func (g SQLGenerator) CreateIndex(schema, table, name string, isClustered bool, fields []IndexField) string {
	clustered := "NONCLUSTERED"

	if isClustered {
		clustered = "CLUSTERED"
	}

	return fmt.Sprintf(`CREATE UNIQUE %s INDEX [%s] ON [%s].[%s] (%s)`, clustered, name, schema, table, indexFieldsDefinition(fields))

}

func (g SQLGenerator) CreateTable(table Table) string {
	name := table.Name
	fields := table.Fields
	schema := table.Schema
	indexes := table.Indexes

	fieldDefs := make([]string, 0, len(fields)+1)

	if table.Recnum {
		fieldDefs = append(fieldDefs, g.fieldDefinition(name, Field{
			Name: "RECNUM",
			Type: "identity",
		}))
	}

	for _, field := range fields {
		fieldDefs = append(fieldDefs, g.fieldDefinition(name, field))
	}

	clusteredIndex := clusteredIndex(name, indexes)

	return fmt.Sprintf(`CREATE TABLE [%s].[%s] (
    %s
    %s
)`, schema, name, strings.Join(fieldDefs, ",\n    "), clusteredIndex)
}

func (g SQLGenerator) AddField(schema, table string, field Field) string {
	return fmt.Sprintf(`ALTER TABLE [%s].[%s] ADD %s;`, schema, table, g.fieldDefinition(table, field))
}

func (g SQLGenerator) AlterField(schema, table string, from, to Field) string {

	return fmt.Sprintf(`ALTER TABLE [%s].[%s] ALTER COLUMN [%s] %s;`, schema, table, from.Name, fieldType(to, g.types))
}

func (g SQLGenerator) DropField(schema, table, field string) string {
	return fmt.Sprintf(`ALTER TABLE [%s].[%s] DROP [%s];`, schema, table, field)
}

func (g SQLGenerator) RenameField(schema, table, from, to string) string {
	return fmt.Sprintf(`EXEC sp_rename @objname=N'[%s].[%s].[%s]', @newname=N'%s', @objtype=N'COLUMN';`, schema, table, from, to)
}
