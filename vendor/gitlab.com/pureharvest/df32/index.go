package df32

type Index struct {
	Number     int
	TableName  string
	Name       string
	Clustered  bool
	PrimaryKey bool `json:"primary_key"`
	Fields     []IndexField
}

type IndexField struct {
	Name       string
	Number     int
	Uppercase  bool
	Descending bool
}
