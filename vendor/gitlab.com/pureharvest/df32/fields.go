package df32

import (
	"fmt"
	"strconv"
	"strings"
)

func NewFieldSizeFromString(s string) (FieldSize, error) {

	scale, precision, err := scaleAndPrecision(s)

	if err != nil {
		return FieldSize{}, err
	}

	return FieldSize{
		Precision: scale,
		Length:    precision,
	}, nil

}

type FieldSize struct {
	Precision int
	Length    int
}

func (f FieldSize) String() string {

	if f.Precision == 0 {
		return fmt.Sprintf("%d", f.Length)
	}

	return fmt.Sprintf("%d.%d", f.Length, f.Precision)
}

func (f *FieldSize) UnmarshalJSON(data []byte) error {
	s := strings.Trim(string(data), `"`)

	scale, precision, err := scaleAndPrecision(s)

	if err != nil {
		return err
	}

	f.Precision = scale
	f.Length = precision

	return nil
}

func scaleAndPrecision(f string) (scale int, precision int, err error) {

	parts := strings.Split(f, ".")

	precision, err = strconv.Atoi(parts[0])

	if err != nil {
		return 0, 0, err
	}

	if len(parts) > 1 {

		scale, err = strconv.Atoi(parts[1])

		if err != nil {
			return 0, 0, err
		}
	}

	return scale, precision, nil
}

func (f FieldSize) SQLLength() int {

	return f.Length + f.Precision
}

type FieldRelation struct {
	File  int
	Field int
}

type Field struct {
	Name      string
	Number    int
	Type      string
	Size      FieldSize
	RelatesTo FieldRelation `json:"relates_to"`

	Index int `json:"use_index"`
}

func (f Field) NeedsIntermediateLength() bool {
	return (f.Type == "number" && f.Size.Precision == 0 && f.Size.Length <= 6)
}

func (f Field) NeedsIntermediate() bool {
	return f.RelatesTo.File != 0 ||
		f.Index != 0 ||
		f.NeedsIntermediateLength()
}
