package df32

import (
	"strings"
	"testing"

	"github.com/ghodss/yaml"

	"bytes"

	"github.com/stretchr/testify/assert"
)

func Test_LoadTableInt(t *testing.T) {

	tests := []struct {
		name  string
		file  string
		table IntermediateFile
		err   error
	}{
		{
			name: "bline",
			file: `DRIVER_NAME MSSQLDRV
SERVER_NAME DFCONNID=POSONA;
DATABASE_NAME BLINE
SCHEMA_NAME dataflex

RECNUM_TABLE YES
PRIMARY_INDEX 0
GENERATE_RECORD_ID_METHOD IDENTITY_COLUMN
TABLE_CHARACTER_FORMAT ANSI
USE_DUMMY_ZERO_DATE YES

FIELD_NUMBER 1
FIELD_INDEX 1

FIELD_NUMBER 2
FIELD_INDEX 1

FIELD_NUMBER 4
FIELD_INDEX 1

FIELD_NUMBER 7
FIELD_RELATED_FILE 30
FIELD_RELATED_FIELD 1

FIELD_NUMBER 13
FIELD_INDEX 3

INDEX_NUMBER 0
INDEX_NAME BLINE000

INDEX_NUMBER 1
INDEX_NAME BLINE001_PK

INDEX_NUMBER 2
INDEX_NAME BLINE002

INDEX_NUMBER 3
INDEX_NAME BLINE003`,

			table: IntermediateFile{
				Server:             "DFCONNID=POSONA;",
				DatabaseName:       "BLINE",
				Schema:             "dataflex",
				Recnum:             true,
				PrimaryIndexNumber: 0,
				RecordIdentity:     "IDENTITY_COLUMN",
				Format:             "ANSI",
				UseDummyZeroDate:   true,
				Fields: []Field{
					{Number: 1, Index: 1},
					{Number: 2, Index: 1},
					{Number: 4, Index: 1},
					{Number: 7, RelatesTo: FieldRelation{File: 30, Field: 1}},
					{Number: 13, Index: 3},
				},
				Indexes: []Index{
					{Number: 0, Name: "BLINE000"},
					{Number: 1, Name: "BLINE001_PK"},
					{Number: 2, Name: "BLINE002"},
					{Number: 3, Name: "BLINE003"},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			table, err := loadIntermediateFile(bytes.NewReader([]byte(test.file)))

			assert.Equal(t, test.err, err)
			assert.Equal(t, test.table, table)
		})
	}
}

func Test_WriteTableInt(t *testing.T) {
	tests := []struct {
		table    IntermediateFile
		expected []byte
		err      error
	}{
		{
			table: IntermediateFile{
				Server:             "DFCONNID=POSONA;",
				DatabaseName:       "BLINE",
				Schema:             "dataflex",
				Recnum:             true,
				PrimaryIndexNumber: 0,
				RecordIdentity:     "IDENTITY_COLUMN",
				Format:             "ANSI",
				UseDummyZeroDate:   true,
				Fields: []Field{
					{Number: 1, Index: 1},
					{Number: 7, RelatesTo: FieldRelation{File: 30, Field: 1}},
				},
				Indexes: []Index{
					{Number: 0, Name: "BLINE000"},
					{Number: 1, Name: "BLINE001_PK"},
				},
			},
			expected: []byte(`DRIVER_NAME MSSQLDRV
SERVER_NAME DFCONNID=POSONA;
DATABASE_NAME BLINE
SCHEMA_NAME dataflex

RECNUM_TABLE YES
PRIMARY_INDEX 0
GENERATE_RECORD_ID_METHOD IDENTITY_COLUMN
TABLE_CHARACTER_FORMAT ANSI
USE_DUMMY_ZERO_DATE YES

FIELD_NUMBER 1
FIELD_INDEX 1

FIELD_NUMBER 7
FIELD_RELATED_FILE 30
FIELD_RELATED_FIELD 1

INDEX_NUMBER 0
INDEX_NAME BLINE000

INDEX_NUMBER 1
INDEX_NAME BLINE001_PK
`),
		},
	}

	for _, test := range tests {
		t.Run(test.table.DatabaseName, func(t *testing.T) {

			var out bytes.Buffer

			err := writeIntermediateFile(test.table, &out)

			if err != nil && test.err == nil {
				t.Fatal(err)
			}
			assert.Equal(t, test.err, err)

			assert.Equal(t, strings.Split(string(test.expected), "\n"), strings.Split(string(out.Bytes()), "\n"))
		})
	}
}

func Test_YamlUnmarshal(t *testing.T) {

	tests := []struct {
		name     string
		input    []byte
		expected Table
	}{
		{
			name: "includes intermediate file",
			input: []byte(`
name: BLINE
file_num: 85
recnum: true
schema: test
display_name: Barcode Scanning Line Item File
fields:
- name: NUMBER
  type: ascii
  size: 10
  use_index: 1
indexes:
- name: BLINE001_PK
  clustered: true
  fields:
  - name: BARCODE
    descending: NO
    uppercase: YES
`),

			expected: Table{
				Name:        "BLINE",
				Number:      85,
				DisplayName: "Barcode Scanning Line Item File",
				Fields: []Field{
					{Name: "NUMBER", Type: "ascii", Size: FieldSize{Length: 10}, Index: 1},
				},
				Indexes: []Index{
					{
						Name:      "BLINE001_PK",
						Clustered: true,
						Fields: []IndexField{
							{Name: "BARCODE", Descending: false, Uppercase: true},
						},
					},
				},
				IntermediateFile: IntermediateFile{
					Schema: "test",
					Recnum: true,
				},
			},
		},
		{
			name: "no intermediate file",
			input: []byte(`
---
name: test
display_name: Test Name
file_num: 20
`),
			expected: Table{
				Name:        "test",
				Number:      20,
				DisplayName: "Test Name",
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			table := Table{}

			if err := yaml.Unmarshal(test.input, &table); err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expected, table)
		})
	}
}

func Test_FileDefinition(t *testing.T) {

	tests := []struct {
		name     string
		table    Table
		expected string
	}{
		{
			name: "basic table",
			table: Table{
				Number: 20,
				Name:   "TEST",
				Fields: []Field{
					{Name: "FIELD1", Type: "ascii"},
					{Name: "FIELD2", Type: "date"},
					{Name: "FIELD3", Type: "number"},
				},
			},
			expected: `#REPLACE FILE20 TEST
#REPLACE TEST.RECNUM |FN20,0
#REPLACE TEST.FIELD1 |FS20,1
#REPLACE TEST.FIELD2 |FD20,2
#REPLACE TEST.FIELD3 |FN20,3
`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			var buf bytes.Buffer

			if err := test.table.FileDefinition(&buf); err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expected, buf.String())
		})
	}
}
