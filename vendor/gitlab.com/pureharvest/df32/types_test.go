package df32

import "testing"
import "github.com/stretchr/testify/assert"

func Test_DefaultTypeMaps(t *testing.T) {

	types := []struct {
		in  string
		out string
	}{
		{
			in:  "date",
			out: "DATE",
		},
		{
			in:  "ascii",
			out: "NVARCHAR",
		},
		{
			in:  "number",
			out: "NUMERIC",
		},
		{
			in:  "datetime",
			out: "DATETIME2",
		},
		{
			in:  "text",
			out: "NTEXT",
		},
	}

	for _, test := range types {
		t.Run(test.in, func(t *testing.T) {
			m := TypeMap{}
			assert.Equal(t, test.out, m.ToSQL(test.in))
		})
	}
}

func Test_TypeMaps(t *testing.T) {

	m := TypeMap{
		ascii:    "a",
		date:     "d",
		datetime: "dt",
		number:   "n",
		text:     "t",
		binary:   "b",
	}

	assert.Equal(t, "A", m.ToSQL("ascii"))
	assert.Equal(t, "D", m.ToSQL("date"))
	assert.Equal(t, "DT", m.ToSQL("datetime"))
	assert.Equal(t, "N", m.ToSQL("number"))
	assert.Equal(t, "T", m.ToSQL("text"))
	assert.Equal(t, "B", m.ToSQL("binary"))
}
