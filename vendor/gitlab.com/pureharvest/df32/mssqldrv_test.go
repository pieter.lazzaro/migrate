package df32

import (
	"database/sql"
	"testing"

	"bytes"

	"github.com/stretchr/testify/assert"
)

func Test_ConvertDataflexToMSSQL(t *testing.T) {
	tests := []struct {
		input  string
		output string
	}{
		{
			input:  "server=(local);UID=sa;PWD=1234%2",
			output: "server=(local);user id=sa;password=1234%2",
		},
		{
			input:  "server=(local);uid=sa;pwd=1234%2",
			output: "server=(local);user id=sa;password=1234%2",
		},
		{
			input:  "server=localhost,1433;UID=sa;PWD=1234%2",
			output: "server=localhost;port=1433;user id=sa;password=1234%2",
		},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			assert.Equal(t, test.output, ConvertDataflexConnectionString(test.input))
		})
	}
}

func Test_MSSQLDRVLoadConfig(t *testing.T) {
	paths := []string{"./test-fixtures"}

	drv, err := NewMSSQLDRV(paths)

	if err != nil {
		t.Fatal(err)
	}

	expected := MSSQLDRV{
		paths: paths,
		connections: []dbConnection{{
			Name:             "POSONA",
			ConnectionString: "SERVER=(local);Database=pureharvest_test;UID=sa;PWD=test123",
			Silent:           true,
		}},
		types: TypeMap{
			datetime: "datetime2",
			date:     "date",
			ascii:    "nvarchar",
			text:     "ntext",
		},
		defaults: map[string]string{
			"char":     "''",
			"ascii":    "''",
			"number":   "0",
			"date":     "'0001-01-01'",
			"text":     "''",
			"binary":   "0",
			"datetime": "'0001-01-01'",
		},
	}

	assert.Equal(t, expected, *drv)
}

func runSQL(t *testing.T, DFconnID dbConnection, query string) {
	connectionString := ConvertDataflexConnectionString(DFconnID.ConnectionString)
	t.Log(connectionString)

	db, err := sql.Open("mssql", connectionString)

	if err != nil {
		t.Fatal(err)
	}

	if err := db.Ping(); err != nil {
		t.Fatal(err)
	}

	defer db.Close()

	if _, err := db.Exec(query); err != nil {
		t.Fatal(err)
	}

}

func Test_TableLoad(t *testing.T) {
	env, deferFn := CreateTestEnvironment(t)

	defer deferFn()

	runSQL(t, env.Driver.connections[0], `
CREATE TABLE [dataflex].[TEST] (
    [RECNUM] INT IDENTITY (1, 1) NOT NULL,
    [NUMBER] NVARCHAR (10) CONSTRAINT [DF__TEST__NUMBER] DEFAULT ('') NOT NULL
    CONSTRAINT [TEST000_PK] PRIMARY KEY CLUSTERED ([RECNUM] ASC)
)
`)

	intFile := []byte(`
DRIVER_NAME MSSQLDRV
SERVER_NAME DFCONNID=POSONA;
DATABASE_NAME TEST
SCHEMA_NAME dataflex

RECNUM_TABLE YES
PRIMARY_INDEX 0
GENERATE_RECORD_ID_METHOD IDENTITY_COLUMN
TABLE_CHARACTER_FORMAT ANSI
USE_DUMMY_ZERO_DATE YES

FIELD_NUMBER 1
FIELD_INDEX 1

INDEX_NUMBER 0
INDEX_NAME TEST000_PK

`)
	table := Table{
		Name: "TEST",
	}

	intTable, err := loadIntermediateFile(bytes.NewReader(intFile))

	if err != nil {
		t.Fatal(err)
	}

	table.IntermediateFile = intTable

	if err := env.Driver.loadTableStructure(&table); err != nil {
		t.Fatal(err)
	}

	expected := Table{
		Name: "TEST",
		IntermediateFile: IntermediateFile{
			DatabaseName:       "TEST",
			Schema:             "dataflex",
			Server:             "DFCONNID=POSONA;",
			Recnum:             true,
			PrimaryIndexNumber: 0,
			RecordIdentity:     "IDENTITY_COLUMN",
			Format:             "ANSI",
			UseDummyZeroDate:   true,
			Fields: []Field{
				{Number: 1, Index: 1},
			},
			Indexes: []Index{
				{Name: "TEST000_PK", Number: 0},
			},
		},
		Fields: []Field{
			{Number: 1, Name: "NUMBER", Type: "ascii", Index: 1, Size: FieldSize{Length: 10}},
		},
		Indexes: []Index{
			{Name: "TEST000_PK", Clustered: true, Number: 0, Fields: []IndexField{{Name: "RECNUM", Descending: false, Number: 1}}},
		},
	}
	assert.Equal(t, expected, table)
}

func Test_TableLoadNotUsedIndexes(t *testing.T) {
	env, deferFn := CreateTestEnvironment(t)

	defer deferFn()

	runSQL(t, env.Driver.connections[0], `
CREATE TABLE [dataflex].[TEST] (
    [RECNUM] INT IDENTITY (1, 1) NOT NULL,
    [NUMBER] NVARCHAR (10) CONSTRAINT [DF__TEST__NUMBER] DEFAULT ('') NOT NULL
    CONSTRAINT [TEST000_PK] PRIMARY KEY CLUSTERED ([RECNUM] ASC)
);

CREATE UNIQUE NONCLUSTERED INDEX [TEST001] ON [dataflex].[TEST] ([NUMBER] DESC)
`)

	intFile := []byte(`
DRIVER_NAME MSSQLDRV
SERVER_NAME DFCONNID=POSONA;
DATABASE_NAME TEST
SCHEMA_NAME dataflex

RECNUM_TABLE YES
PRIMARY_INDEX 0
GENERATE_RECORD_ID_METHOD IDENTITY_COLUMN
TABLE_CHARACTER_FORMAT ANSI
USE_DUMMY_ZERO_DATE YES

FIELD_NUMBER 1
FIELD_INDEX 1

INDEX_NUMBER 0
INDEX_NAME TEST000_PK

`)
	table := Table{
		Name: "TEST",
	}

	intTable, err := loadIntermediateFile(bytes.NewReader(intFile))

	if err != nil {
		t.Fatal(err)
	}

	table.IntermediateFile = intTable

	if err := env.Driver.loadTableStructure(&table); err != nil {
		t.Fatal(err)
	}
	expected := Table{
		Name: "TEST",
		IntermediateFile: IntermediateFile{
			DatabaseName:       "TEST",
			Schema:             "dataflex",
			Server:             "DFCONNID=POSONA;",
			Recnum:             true,
			PrimaryIndexNumber: 0,
			RecordIdentity:     "IDENTITY_COLUMN",
			Format:             "ANSI",
			UseDummyZeroDate:   true,
			Fields: []Field{
				{Number: 1, Index: 1},
			},
			Indexes: []Index{
				{Name: "TEST000_PK", Number: 0},
			},
		},
		Fields: []Field{
			{Number: 1, Name: "NUMBER", Type: "ascii", Index: 1, Size: FieldSize{Length: 10}},
		},
		Indexes: []Index{
			{Name: "TEST000_PK", Clustered: true, Number: 0, Fields: []IndexField{{Name: "RECNUM", Descending: false, Number: 1}}},
		},
	}
	assert.Equal(t, expected, table)
}

func Test_TableLoadIndexes(t *testing.T) {
	env, deferFn := CreateTestEnvironment(t)

	defer deferFn()

	runSQL(t, env.Driver.connections[0], `
CREATE TABLE [dataflex].[TEST] (
    [RECNUM] INT IDENTITY (1, 1) NOT NULL,
    [NUMBER] NVARCHAR (10) CONSTRAINT [DF__TEST__NUMBER] DEFAULT ('') NOT NULL
    CONSTRAINT [TEST000_PK] PRIMARY KEY CLUSTERED ([RECNUM] ASC)
);

CREATE UNIQUE NONCLUSTERED INDEX [TEST001] ON [dataflex].[TEST] ([NUMBER] DESC)
`)

	intFile := []byte(`
DRIVER_NAME MSSQLDRV
SERVER_NAME DFCONNID=POSONA;
DATABASE_NAME TEST
SCHEMA_NAME dataflex

RECNUM_TABLE YES
PRIMARY_INDEX 0
GENERATE_RECORD_ID_METHOD IDENTITY_COLUMN
TABLE_CHARACTER_FORMAT ANSI
USE_DUMMY_ZERO_DATE YES

FIELD_NUMBER 1
FIELD_INDEX 1

INDEX_NUMBER 0
INDEX_NAME TEST000_PK

INDEX_NUMBER 1
INDEX_NAME TEST001
`)
	table := Table{
		Name: "TEST",
	}

	intTable, err := loadIntermediateFile(bytes.NewReader(intFile))

	if err != nil {
		t.Fatal(err)
	}

	table.IntermediateFile = intTable

	if err := env.Driver.loadTableStructure(&table); err != nil {
		t.Fatal(err)
	}
	expected := Table{
		Name: "TEST",
		IntermediateFile: IntermediateFile{
			DatabaseName:       "TEST",
			Schema:             "dataflex",
			Server:             "DFCONNID=POSONA;",
			Recnum:             true,
			PrimaryIndexNumber: 0,
			RecordIdentity:     "IDENTITY_COLUMN",
			Format:             "ANSI",
			UseDummyZeroDate:   true,
			Fields: []Field{
				{Number: 1, Index: 1},
			},
			Indexes: []Index{
				{Name: "TEST000_PK", Number: 0},
				{Name: "TEST001", Number: 1},
			},
		},
		Fields: []Field{
			{Number: 1, Name: "NUMBER", Type: "ascii", Index: 1, Size: FieldSize{Length: 10}},
		},
		Indexes: []Index{
			{Name: "TEST000_PK", Clustered: true, Number: 0, Fields: []IndexField{{Name: "RECNUM", Descending: false, Number: 1}}},
			{Name: "TEST001", Clustered: false, Number: 1, Fields: []IndexField{{Name: "NUMBER", Descending: true, Number: 1}}},
		},
	}
	assert.Equal(t, expected, table)
}

func Test_TableLoadLegacyTypes(t *testing.T) {
	env, deferFn := CreateTestEnvironment(t)

	defer deferFn()

	runSQL(t, env.Driver.connections[0], `
CREATE TABLE [dataflex].[TEST] (
    [RECNUM] INT IDENTITY (1, 1) NOT NULL,
    [NUMBER] CHAR (10) CONSTRAINT [DF__TEST__NUMBER] DEFAULT ('') NOT NULL,
	[DATE_FIELD] DATE NOT NULL,
	[INT_FIELD] INT NOT NULL,
	[SMALLINT_FIELD] SMALLINT NOT NULL,
	[DATETIME_FIELD] DATETIME NOT NULL
    CONSTRAINT [TEST000_PK] PRIMARY KEY CLUSTERED ([RECNUM] ASC)
)
`)

	intFile := []byte(`
DRIVER_NAME MSSQLDRV
SERVER_NAME DFCONNID=POSONA;
DATABASE_NAME TEST
SCHEMA_NAME dataflex

RECNUM_TABLE YES
PRIMARY_INDEX 0
GENERATE_RECORD_ID_METHOD IDENTITY_COLUMN
TABLE_CHARACTER_FORMAT ANSI
USE_DUMMY_ZERO_DATE YES

FIELD_NUMBER 1
FIELD_INDEX 1

FIELD_NUMBER 3
FIELD_LENGTH 6

FIELD_NUMBER 4
FIELD_LENGTH 2

INDEX_NUMBER 0
INDEX_NAME TEST000_PK

`)
	table := Table{
		Name: "TEST",
	}

	intTable, err := loadIntermediateFile(bytes.NewReader(intFile))

	if err != nil {
		t.Fatal(err)
	}

	table.IntermediateFile = intTable

	if err := env.Driver.loadTableStructure(&table); err != nil {
		t.Fatal(err)
	}

	expected := Table{
		Name: "TEST",
		IntermediateFile: IntermediateFile{
			DatabaseName:       "TEST",
			Schema:             "dataflex",
			Server:             "DFCONNID=POSONA;",
			Recnum:             true,
			PrimaryIndexNumber: 0,
			RecordIdentity:     "IDENTITY_COLUMN",
			Format:             "ANSI",
			UseDummyZeroDate:   true,
			Fields: []Field{
				{Number: 1, Index: 1},
				{Number: 3, Size: FieldSize{Length: 6}},
				{Number: 4, Size: FieldSize{Length: 2}},
			},
			Indexes: []Index{
				{Name: "TEST000_PK", Number: 0},
			},
		},
		Fields: []Field{
			{Number: 1, Name: "NUMBER", Type: "ascii", Index: 1, Size: FieldSize{Length: 10}},
			{Number: 2, Name: "DATE_FIELD", Type: "date"},
			{Number: 3, Name: "INT_FIELD", Type: "number", Size: FieldSize{Length: 6}},
			{Number: 4, Name: "SMALLINT_FIELD", Type: "number", Size: FieldSize{Length: 2}},
			{Number: 5, Name: "DATETIME_FIELD", Type: "date"},
		},
		Indexes: []Index{
			{Name: "TEST000_PK", Clustered: true, Number: 0, Fields: []IndexField{{Name: "RECNUM", Descending: false, Number: 1}}},
		},
	}
	assert.Equal(t, expected, table)
}

func Test_IntermediateFiles(t *testing.T) {
	tests := []struct {
		name     string
		table    IntermediateFile
		expected string
	}{
		{
			name: "recnum file",
			table: IntermediateFile{
				Recnum:           true,
				DatabaseName:     "TEST",
				Schema:           "dataflex",
				Format:           "ANSI",
				RecordIdentity:   "IDENTITY_COLUMN",
				UseDummyZeroDate: true,
				Server:           "DFCONNID=POSONA;",
			},
			expected: `DRIVER_NAME MSSQLDRV
SERVER_NAME DFCONNID=POSONA;
DATABASE_NAME TEST
SCHEMA_NAME dataflex

RECNUM_TABLE YES
PRIMARY_INDEX 0
GENERATE_RECORD_ID_METHOD IDENTITY_COLUMN
TABLE_CHARACTER_FORMAT ANSI
USE_DUMMY_ZERO_DATE YES
`,
		},
		{
			name: "system file",
			table: IntermediateFile{
				System:           true,
				DatabaseName:     "TEST",
				Schema:           "dataflex",
				Format:           "ANSI",
				UseDummyZeroDate: true,
				Server:           "DFCONNID=POSONA;",
			},
			expected: `DRIVER_NAME MSSQLDRV
SERVER_NAME DFCONNID=POSONA;
DATABASE_NAME TEST
SCHEMA_NAME dataflex

SYSTEM_FILE YES
TABLE_CHARACTER_FORMAT ANSI
USE_DUMMY_ZERO_DATE YES
`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			var buf bytes.Buffer

			if err := writeIntermediateFile(test.table, &buf); err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expected, buf.String())
		})
	}
}
