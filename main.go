// Package main is the CLI.
// You can use the CLI via Terminal.
// import "gitlab.com/pieter.lazzaro/migrate/migrate" for usage within Go.
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"context"

	_ "gitlab.com/pieter.lazzaro/migrate/driver/bash"
	_ "gitlab.com/pieter.lazzaro/migrate/driver/cassandra"
	_ "gitlab.com/pieter.lazzaro/migrate/driver/dataflex"
	_ "gitlab.com/pieter.lazzaro/migrate/driver/mysql"
	_ "gitlab.com/pieter.lazzaro/migrate/driver/postgres"
	_ "gitlab.com/pieter.lazzaro/migrate/driver/sqlite3"
	"gitlab.com/pieter.lazzaro/migrate/migrate"
)

// var url = flag.String("url", os.Getenv("MIGRATE_URL"), "")
var migrationsPath = flag.String("path", "", "")
var version = flag.Bool("version", false, "Show migrate version")
var migrator = migrate.Migrator{}

func main() {
	flag.Usage = func() {
		helpCmd()
	}

	flag.Var(&migrator, "url", "")

	flag.Parse()
	command := flag.Arg(0)
	if *version {
		fmt.Println(Version)
		os.Exit(0)
	}

	if *migrationsPath == "" {
		*migrationsPath, _ = os.Getwd()
	}

	defer migrator.CloseDrivers()

	switch command {
	case "create":
		verifyMigrationsPath(*migrationsPath)
		name := flag.Arg(1)
		if name == "" {
			fmt.Println("Please specify name.")
			os.Exit(1)
		}

		migrationFile, err := migrator.Create(*migrationsPath, name, "")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		fmt.Printf("Version %v migration files created in %v:\n", migrationFile.Version, *migrationsPath)
		fmt.Println(migrationFile.UpFile.FileName)
		fmt.Println(migrationFile.DownFile.FileName)

	case "migrate":
		verifyMigrationsPath(*migrationsPath)
		relativeN := flag.Arg(1)
		relativeNInt, err := strconv.Atoi(relativeN)
		if err != nil {
			fmt.Println("Unable to parse param <n>.")
			os.Exit(1)
		}
		timerStart = time.Now()
		ctx := context.Background()

		if err := migrator.Migrate(ctx, *migrationsPath, relativeNInt); err != nil {
			log.Println(err)
			os.Exit(1)
		}

	case "goto":
		verifyMigrationsPath(*migrationsPath)
		toVersion := flag.Arg(1)
		toVersionInt, err := strconv.Atoi(toVersion)
		if err != nil || toVersionInt < 0 {
			fmt.Println("Unable to parse param <v>.")
			os.Exit(1)
		}

		currentVersion, err := migrator.Version()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		relativeNInt := toVersionInt - int(currentVersion)

		timerStart = time.Now()
		ctx := context.Background()

		if err := migrator.Migrate(ctx, *migrationsPath, relativeNInt); err != nil {
			log.Println(err)
			os.Exit(1)
		}

	case "up":
		verifyMigrationsPath(*migrationsPath)
		timerStart = time.Now()
		ctx := context.Background()

		if err := migrator.Up(ctx, *migrationsPath); err != nil {
			log.Println(err)
			os.Exit(1)
		}

	case "down":
		verifyMigrationsPath(*migrationsPath)
		timerStart = time.Now()
		ctx := context.Background()

		if err := migrator.Down(ctx, *migrationsPath); err != nil {
			log.Println(err)
			os.Exit(1)
		}

	case "redo":
		verifyMigrationsPath(*migrationsPath)
		timerStart = time.Now()
		ctx := context.Background()

		if err := migrator.Redo(ctx, *migrationsPath); err != nil {
			log.Println(err)
			os.Exit(1)
		}

	case "reset":
		verifyMigrationsPath(*migrationsPath)
		timerStart = time.Now()
		ctx := context.Background()

		if err := migrator.Reset(ctx, *migrationsPath); err != nil {
			log.Println(err)
			os.Exit(1)
		}

	case "version":
		verifyMigrationsPath(*migrationsPath)
		version, err := migrator.Version()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		fmt.Println(version)

	case "list":
		verifyMigrationsPath(*migrationsPath)
		files, err := migrator.ListFiles(*migrationsPath)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		for _, file := range files {
			fmt.Printf("%d: %s", file.Version, file.UpFile.Name)
		}
	case "baseline":
		version := flag.Arg(1)
		versionInt, err := strconv.ParseUint(version, 10, 64)
		if err != nil {
			fmt.Println("Unable to parse param <n>.")
			os.Exit(1)
		}

		if err := migrator.Baseline(versionInt); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	default:
		helpCmd()
		os.Exit(1)
	case "help":
		helpCmd()
	}
}

func verifyMigrationsPath(path string) {
	if path == "" {
		fmt.Println("Please specify path")
		os.Exit(1)
	}
}

var timerStart time.Time

func printTimer() {
	diff := time.Now().Sub(timerStart).Seconds()
	if diff > 60 {
		fmt.Printf("\n%.4f minutes\n", diff/60)
	} else {
		fmt.Printf("\n%.4f seconds\n", diff)
	}
}

func helpCmd() {
	os.Stderr.WriteString(
		`usage: migrate [-path=<path>] -url=<url> <command> [<args>]

Commands:
   create <name>  Create a new migration
   baseline <n>	  Baseline the database to version n.
   up             Apply all -up- migrations
   down           Apply all -down- migrations
   reset          Down followed by Up
   redo           Roll back most recent migration, then apply it again
   version        Show current migration version
   migrate <n>    Apply migrations -n|+n
   goto <v>       Migrate to version v
   help           Show this help

'-path' defaults to current working directory.
`)
}
