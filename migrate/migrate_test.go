package migrate

import (
	"context"
	"fmt"
	"io/ioutil"
	"testing"

	// Ensure imports for each driver we wish to test

	"os"

	"gitlab.com/pieter.lazzaro/migrate/driver"
	testdriver "gitlab.com/pieter.lazzaro/migrate/driver/test"
)

var driverURL = "testdriver://test"

func TestCreate(t *testing.T) {

	m, tmpdir, deferFunc := createMigrator(t, 0)

	defer deferFunc()

	if _, err := m.Create(tmpdir, "test_migration", "sql"); err != nil {
		t.Fatal(err)
	}
	if _, err := m.Create(tmpdir, "another migration", "sql"); err != nil {
		t.Fatal(err)
	}

	files, err := ioutil.ReadDir(tmpdir)
	if err != nil {
		t.Fatal(err)
	}
	if len(files) != 4 {
		t.Fatal("Expected 2 new files, got", len(files))
	}
	expectFiles := []string{
		"0001_test_migration.up.sql", "0001_test_migration.down.sql",
		"0002_another_migration.up.sql", "0002_another_migration.down.sql",
	}
	foundCounter := 0
	for _, expectFile := range expectFiles {
		for _, file := range files {
			if expectFile == file.Name() {
				foundCounter++
				break
			}
		}
	}
	if foundCounter != len(expectFiles) {
		t.Error("not all expected files have been found")
	}

}

func createMigrations(t *testing.T, m *Migrator, tmpdir string, toMake int) {
	for i := 0; i < toMake; i++ {
		m.Create(tmpdir, fmt.Sprintf("migration%d", i+1), "sql")
	}
}

func createMigrator(t *testing.T, version uint64) (*Migrator, string, func()) {
	var driver1 = testdriver.New(t, "sql", version)
	driver.RegisterDriver("testdriver", driver1)

	m, err := New(driverURL)

	if err != nil {
		t.Fatal(err)
	}

	tmpdir, err := ioutil.TempDir(os.TempDir(), "migrate-test")
	if err != nil {
		t.Fatal(err)
	}

	return m, tmpdir, func() {
		driver.UnregisterDriver("testdriver")
		os.RemoveAll(tmpdir)
	}

}

func TestReset(t *testing.T) {

	m, tmpdir, deferFunc := createMigrator(t, 0)

	defer deferFunc()

	createMigrations(t, m, tmpdir, 2)

	if err := m.Reset(context.Background(), tmpdir); err != nil {
		t.Fatal(err)
	}

	testdriver.AssertVersion(t, m, 2)

}

func TestDown(t *testing.T) {

	m, tmpdir, deferFunc := createMigrator(t, 2)

	defer deferFunc()

	createMigrations(t, m, tmpdir, 2)

	if err := m.Down(context.Background(), tmpdir); err != nil {
		t.Fatal(err)
	}
	testdriver.AssertVersion(t, m, 0)
}

func TestUp(t *testing.T) {

	m, tmpdir, deferFunc := createMigrator(t, 0)

	defer deferFunc()

	createMigrations(t, m, tmpdir, 2)

	if err := m.Up(context.Background(), tmpdir); err != nil {
		t.Fatal(err)
	}

	testdriver.AssertVersion(t, m, 2)
}

func TestRedo(t *testing.T) {

	m, tmpdir, deferFunc := createMigrator(t, 2)
	createMigrations(t, m, tmpdir, 2)

	defer deferFunc()

	if err := m.Redo(context.Background(), tmpdir); err != nil {
		t.Fatal(err)
	}
	testdriver.AssertVersion(t, m, 2)

}

func TestMigrate(t *testing.T) {

	m, tmpdir, deferFunc := createMigrator(t, 2)
	createMigrations(t, m, tmpdir, 2)

	defer deferFunc()

	if err := m.Migrate(context.Background(), tmpdir, -2); err != nil {
		t.Fatal(err)
	}
	testdriver.AssertVersion(t, m, 0)

	if err := m.Migrate(context.Background(), tmpdir, +1); err != nil {
		t.Fatal(err)
	}
	testdriver.AssertVersion(t, m, 1)

}
