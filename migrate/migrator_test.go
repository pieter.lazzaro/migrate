package migrate

import (
	"context"
	"testing"

	"io/ioutil"

	"os"

	"gitlab.com/pieter.lazzaro/migrate/driver"
	testdriver "gitlab.com/pieter.lazzaro/migrate/driver/test"
)

func TestMigratorSet(t *testing.T) {
	var driver1 = testdriver.New(t, "test", 0)
	driver.RegisterDriver("driver1", driver1)

	defer driver.UnregisterDriver("driver1")

	migrator := Migrator{}

	if err := migrator.Set("driver1://test"); err != nil {
		t.Fatal("Should be able to set a url")
	}

}

func TestMigratorSetMultiple(t *testing.T) {
	var driver1 = testdriver.New(t, "test", 0)
	var driver2 = testdriver.New(t, "test2", 0)
	driver.RegisterDriver("driver1", driver1)
	driver.RegisterDriver("driver2", driver2)

	defer driver.UnregisterDriver("driver1")
	defer driver.UnregisterDriver("driver2")

	migrator, _ := New("driver1://test1")
	if err := migrator.Set("driver2://test2"); err != nil {
		t.Fatal("Should be able to set mutiple urls", err)
	}

}

func TestMigratorCantSetMultipleWithSameExtension(t *testing.T) {
	var driver1 = testdriver.New(t, "test", 0)
	var driver2 = testdriver.New(t, "test", 0)
	driver.RegisterDriver("driver1", driver1)
	driver.RegisterDriver("driver2", driver2)

	defer driver.UnregisterDriver("driver1")
	defer driver.UnregisterDriver("driver2")
	_, err := New("driver1://test", "driver2://test")
	if err == nil {
		t.Fatal("Should not be able to set mutiple urls which register the same extension")
	}

}

func TestMigratorWillGetTheHighestVersion(t *testing.T) {
	var driver1 = testdriver.New(t, "test", 2)
	var driver2 = testdriver.New(t, "test2", 3)
	driver.RegisterDriver("driver1", driver1)
	driver.RegisterDriver("driver2", driver2)

	defer driver.UnregisterDriver("driver1")
	defer driver.UnregisterDriver("driver2")

	migrator, _ := New("driver1://test", "driver2://test")

	if v, err := migrator.Version(); err != nil || v != 3 {
		t.Fatalf("Expected version to be %d got %d. %s", 3, v, err)
	}
}

func TestMigratorGetsNextVersion(t *testing.T) {
	tmpdir, err := ioutil.TempDir(os.TempDir(), "migrate-test")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(tmpdir)
	if err = ioutil.WriteFile(tmpdir+"/0001_test.up.test", nil, 0644); err != nil {
		t.Fatal(err)
	}
	if err = ioutil.WriteFile(tmpdir+"/0002_test.up.test2", nil, 0644); err != nil {
		t.Fatal(err)
	}

	var driver1 = testdriver.New(t, "test", 0)
	var driver2 = testdriver.New(t, "test2", 0)
	driver.RegisterDriver("driver1", driver1)
	driver.RegisterDriver("driver2", driver2)

	defer driver.UnregisterDriver("driver1")
	defer driver.UnregisterDriver("driver2")

	migrator, _ := New("driver1://test", "driver2://test")

	if v, err := migrator.NextVersion(tmpdir); err != nil || v != 3 {
		t.Fatalf("Expected version to be %d got %d. %s", 3, v, err)
	}

}

func TestMigratorAsyncCancel(t *testing.T) {
	var driver1 = testdriver.NewAsync(t, "test", 0)

	driver.RegisterDriver("driver1", driver1)
	defer driver.UnregisterDriver("driver1")

	tmpdir, err := ioutil.TempDir(os.TempDir(), "migrate-test")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(tmpdir)
	if err = ioutil.WriteFile(tmpdir+"/0001_test.up.test", nil, 0644); err != nil {
		t.Fatal(err)
	}
	if err = ioutil.WriteFile(tmpdir+"/0002_test.up.test", nil, 0644); err != nil {
		t.Fatal(err)
	}
	if err = ioutil.WriteFile(tmpdir+"/0003_test.up.test", nil, 0644); err != nil {
		t.Fatal(err)
	}

	migrator, _ := New("driver1://test")

	errs := make(chan error)
	ctx, cancel := context.WithCancel(context.Background())

	go func() { errs <- migrator.Up(ctx, tmpdir); migrator.CloseDrivers() }()

	driver1.ApplyMigration()
	testdriver.AssertVersion(t, migrator, 1)
	cancel()

	<-errs
	testdriver.AssertVersion(t, migrator, 1)
}

func TestMigratorAsync(t *testing.T) {
	var driver1 = testdriver.NewAsync(t, "test", 0)
	driver.RegisterDriver("driver1", driver1)
	defer driver.UnregisterDriver("driver1")

	tmpdir, err := ioutil.TempDir(os.TempDir(), "migrate-test")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(tmpdir)
	if err = ioutil.WriteFile(tmpdir+"/0001_test.up.test", nil, 0644); err != nil {
		t.Fatal(err)
	}
	if err = ioutil.WriteFile(tmpdir+"/0002_test.up.test", nil, 0644); err != nil {
		t.Fatal(err)
	}
	migrator := &Migrator{}
	migrator.Set("driver1://test")

	errs := make(chan error)
	ctx := context.TODO()
	go func() { errs <- migrator.Up(ctx, tmpdir) }()

	testdriver.AssertVersion(t, migrator, 0)
	driver1.ApplyMigration()
	testdriver.AssertVersion(t, migrator, 1)
	driver1.ApplyMigration()
	<-errs
	testdriver.AssertVersion(t, migrator, 2)
}
