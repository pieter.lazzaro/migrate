package migrate

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"strconv"
	"strings"

	"gitlab.com/pieter.lazzaro/migrate/driver"
	"gitlab.com/pieter.lazzaro/migrate/file"
	"gitlab.com/pieter.lazzaro/migrate/migrate/direction"
)

var logger = log.New(os.Stderr, "", log.LstdFlags)

func New(urls ...string) (*Migrator, error) {

	m := Migrator{}

	for _, url := range urls {
		if err := m.AddDriver(url); err != nil {
			return nil, err
		}
	}
	return &m, nil
}

type Migrator struct {
	drivers map[string]driver.Driver
}

func (m *Migrator) AddDriver(url string) error {

	return m.newDriver(url)

}

// Set is an alias for Add so that it conforms to the Flag package Var interface.
func (m *Migrator) Set(url string) error {
	return m.AddDriver(url)
}

func (m *Migrator) newDriver(url string) error {
	if m.drivers == nil {
		m.drivers = make(map[string]driver.Driver)
	}

	d, err := driver.New(url)

	if err != nil {
		return err
	}

	if _, found := m.drivers[d.FilenameExtension()]; found {
		return errors.New("Multiple drivers specified for the same file type.")
	}

	m.drivers[d.FilenameExtension()] = d
	return nil
}

func (m Migrator) String() string {
	return ""
}

// Extensions returns a slice of all the file extensions that have drivers
// registered.
func (m Migrator) Extensions() []string {
	extensions := make([]string, 0, len(m.drivers))

	for ext := range m.drivers {
		extensions = append(extensions, ext)
	}

	return extensions
}

// Version returns the highest version of all migration drivers
func (m *Migrator) Version() (uint64, error) {
	var currentVersion uint64

	for _, d := range m.drivers {
		v, err := d.Version()

		if err != nil {
			return 0, err
		}

		if v > currentVersion {
			currentVersion = v
		}
	}

	return currentVersion, nil
}

// NextVersion finds the next available version from all migration drivers
func (m *Migrator) NextVersion(migrationsPath string) (uint64, error) {

	version := uint64(0)

	if len(m.drivers) < 1 {
		return version, errors.New("No drivers specified.")
	}

	for _, d := range m.drivers {

		files, err := file.ReadMigrationFiles(migrationsPath, file.FilenameRegex(d.FilenameExtension()))

		if err != nil {
			return version, err
		}

		if len(files) > 0 {

			lastFile := files[len(files)-1]

			if lastFile.Version > version {
				version = lastFile.Version
			}
		}
	}
	version++
	return version, nil
}

func (m *Migrator) MigrateFile(ctx context.Context, f file.File) error {
	ext := strings.TrimLeft(path.Ext(f.FileName), ".")

	driver, found := m.drivers[ext]
	if !found {
		return errors.New("Don't know how to handle that file")
	}
	return driver.Migrate(ctx, f, logger)
}

func (m *Migrator) runMigrations(ctx context.Context, files file.Files) error {
	if len(files) > 0 {
		for _, f := range files {
			select {
			case <-ctx.Done():
				return ctx.Err()
			default:
				if err := m.MigrateFile(ctx, f); err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func (m *Migrator) Migrate(ctx context.Context, migrationsPath string, relativeN int) error {
	var (
		files   file.MigrationFiles
		version uint64
		err     error
	)

	if files, err = m.ListFiles(migrationsPath); err != nil {
		return err
	}

	if version, err = m.Version(); err != nil {
		return err
	}

	applyMigrationFiles, err := files.From(version, relativeN)

	if err != nil {
		return err
	}

	return m.runMigrations(ctx, applyMigrationFiles)
}

// Up applies all available migrations
func (m *Migrator) Up(ctx context.Context, migrationsPath string) error {
	var (
		files   file.MigrationFiles
		version uint64
		err     error
	)

	if files, err = m.ListFiles(migrationsPath); err != nil {
		return err
	}

	if version, err = m.Version(); err != nil {
		return err
	}

	applyMigrationFiles, err := files.ToLastFrom(version)

	if err != nil {
		return err
	}

	return m.runMigrations(ctx, applyMigrationFiles)

}

// Down rolls back all migrations
func (m *Migrator) Down(ctx context.Context, migrationsPath string) error {

	var (
		files   file.MigrationFiles
		version uint64
		err     error
	)

	if files, err = m.ListFiles(migrationsPath); err != nil {
		return err
	}

	if version, err = m.Version(); err != nil {
		return err
	}

	applyMigrationFiles, err := files.ToFirstFrom(version)

	if err != nil {
		return err
	}

	return m.runMigrations(ctx, applyMigrationFiles)
}

// Redo rolls back the most recently applied migration, then runs it again.
func (m *Migrator) Redo(ctx context.Context, migrationsPath string) error {
	if err := m.Migrate(ctx, migrationsPath, -1); err != nil {
		return err
	}
	return m.Migrate(ctx, migrationsPath, +1)
}

// Reset runs the all the down migrations and then all the up migrations
func (m *Migrator) Reset(ctx context.Context, migrationsPath string) error {
	if err := m.Down(ctx, migrationsPath); err != nil {
		return err
	}
	return m.Up(ctx, migrationsPath)

}

func (m *Migrator) Baseline(version uint64) error {
	for _, d := range m.drivers {
		err := d.Baseline(version)

		if err != nil {
			return err
		}
	}

	return nil
}

// Create creates a new migration. It will create a up file and a down file.
func (m *Migrator) Create(migrationsPath, name, driverExt string) (*file.MigrationFile, error) {

	version, err := m.NextVersion(migrationsPath)

	if err != nil {
		return nil, err
	}

	versionStr := strconv.FormatUint(version, 10)
	length := 4 // TODO(mattes) check existing files and try to guess length
	if len(versionStr)%length != 0 {
		versionStr = strings.Repeat("0", length-len(versionStr)%length) + versionStr
	}

	filenamef := "%s_%s.%s.%s"
	name = strings.Replace(name, " ", "_", -1)

	mfile := &file.MigrationFile{
		Version: version,
		UpFile: &file.File{
			Path:      migrationsPath,
			FileName:  fmt.Sprintf(filenamef, versionStr, name, "up", driverExt),
			Name:      name,
			Content:   []byte(""),
			Direction: direction.Up,
		},
		DownFile: &file.File{
			Path:      migrationsPath,
			FileName:  fmt.Sprintf(filenamef, versionStr, name, "down", driverExt),
			Name:      name,
			Content:   []byte(""),
			Direction: direction.Down,
		},
	}

	if err := ioutil.WriteFile(path.Join(mfile.UpFile.Path, mfile.UpFile.FileName), mfile.UpFile.Content, 0644); err != nil {
		return nil, err
	}
	if err := ioutil.WriteFile(path.Join(mfile.DownFile.Path, mfile.DownFile.FileName), mfile.DownFile.Content, 0644); err != nil {
		return nil, err
	}

	return mfile, nil
}

func (m *Migrator) ListFiles(migrationsPath string) (file.MigrationFiles, error) {
	extensions := m.Extensions()

	return file.ReadMigrationFiles(
		migrationsPath,
		file.MultipleDriverFilenameRegex(extensions),
	)

}

// CloseDrivers will close all the migration drivers
func (m *Migrator) CloseDrivers() []error {

	var errs []error
	for _, driver := range m.drivers {

		if err := driver.Close(); err != nil {
			errs = append(errs, err)
		}
	}

	return errs

}
