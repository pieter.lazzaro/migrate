// Package test implements a test Driver for use in testing functionality
// The test driver does not autmaticially register itself in the driver registry
// so that you can register it multiple times under different names when testing.
package test

import (
	"context"
	"errors"
	"sync"
	"testing"

	"gitlab.com/pieter.lazzaro/migrate/driver"
	"gitlab.com/pieter.lazzaro/migrate/file"
	"gitlab.com/pieter.lazzaro/migrate/migrate/direction"
)

// Versioner is anything that we can check for a version
type Versioner interface {
	Version() (uint64, error)
}

// AssertVersion is a utility function to test for versions.
func AssertVersion(t *testing.T, d Versioner, version uint64) {
	v, err := d.Version()
	if err != nil {
		t.Fatal(err)
	}
	if version != v {
		t.Fatalf("Expected version %v, got %v", version, v)
	}
}

func NewLogger(t *testing.T) TestLogger {
	return TestLogger{t}
}

type TestLogger struct {
	t *testing.T
}

func (tl TestLogger) Print(args ...interface{}) {
	tl.t.Log(args...)
}

func (tl TestLogger) Printf(format string, args ...interface{}) {
	tl.t.Logf(format, args...)
}

func (tl TestLogger) Println(args ...interface{}) {
	tl.t.Log(args...)
}

// New creates a new test driver which handles files with the provided extension. it
// will also set the version to the provided version. All versions between 0 and version
// will be created as well.
func New(t *testing.T, extension string, version uint64) *Driver {
	driver := Driver{
		ext: extension,
		t:   t,
	}

	driver.setVersion(version)

	return &driver
}

// Driver is a test driver to that can be used for testing out migrations.
type Driver struct {
	open bool
	ext  string

	versionMu sync.Mutex
	versions  map[uint64]bool

	t *testing.T
}

func (driver *Driver) setVersion(version uint64) {
	driver.versionMu.Lock()
	defer driver.versionMu.Unlock()

	driver.versions = make(map[uint64]bool)
	for i := 0; i < int(version)+1; i++ {
		driver.versions[uint64(i)] = true
	}
}

func (driver *Driver) Initialize(url string) error {
	driver.open = true

	return nil
}

func (driver *Driver) Close() error {
	driver.open = false
	return nil
}

func (driver *Driver) FilenameExtension() string {
	return driver.ext
}

func (driver *Driver) Migrate(ctx context.Context, f file.File, log driver.Logger) error {

	if !driver.open {
		return errors.New("Driver is closed")
	}

	driver.versionMu.Lock()
	defer driver.versionMu.Unlock()

	if f.Direction == direction.Up {
		driver.t.Logf("Applying migration %s version %d", f.Name, f.Version)
		driver.versions[f.Version] = true
	}

	if f.Direction == direction.Down {
		driver.t.Logf("Removing migration %s version %d", f.Name, f.Version)
		driver.versions[f.Version] = false
	}
	return nil
}

func (driver *Driver) Version() (uint64, error) {

	version := uint64(0)
	driver.versionMu.Lock()
	defer driver.versionMu.Unlock()

	for v, applied := range driver.versions {
		if applied && v > version {
			version = v
		}
	}
	return version, nil
}

func (driver *Driver) Baseline(version uint64) error {
	driver.versionMu.Lock()
	defer driver.versionMu.Unlock()

	driver.versions[version] = true

	return nil
}

// NewAsync creates an Async test driver.
func NewAsync(t *testing.T, extension string, version uint64) *AsyncDriver {
	driver := New(t, extension, version)

	return &AsyncDriver{
		*driver,
		make(chan struct{}),
	}
}

// AsyncDriver is a test driver that allows control over when a migration finishes.
type AsyncDriver struct {
	Driver
	apply chan struct{}
}

// ApplyMigration will advance the current migration.
func (driver *AsyncDriver) ApplyMigration() {
	driver.apply <- struct{}{}
}

func (driver *AsyncDriver) Migrate(ctx context.Context, f file.File, log driver.Logger) error {

	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-driver.apply:
	}

	return driver.Driver.Migrate(ctx, f, log)
}
