package test

import (
	"context"
	"testing"

	"gitlab.com/pieter.lazzaro/migrate/file"
	"gitlab.com/pieter.lazzaro/migrate/migrate/direction"
)

func TestDriverCanMigrateUp(t *testing.T) {

	driver := New(t, "", 0)

	driver.Initialize("")

	f := file.File{
		Direction: direction.Up,
		Version:   uint64(1),
	}

	if err := driver.Migrate(context.TODO(), f, NewLogger(t)); err != nil {
		t.Fatal(err)
	}

	AssertVersion(t, driver, 1)

}

func TestDriverCanMigrateDown(t *testing.T) {

	driver := New(t, "", 1)

	driver.Initialize("")

	f := file.File{
		Direction: direction.Down,
		Version:   uint64(1),
	}

	if err := driver.Migrate(context.TODO(), f, NewLogger(t)); err != nil {
		t.Fatal(err)
	}

	AssertVersion(t, driver, 0)

}

func TestDriverWontMigrateAfterClose(t *testing.T) {
	driver := New(t, "", 1)

	driver.Initialize("")

	_ = driver.Close()

	f := file.File{
		Direction: direction.Down,
		Version:   uint64(1),
	}

	if err := driver.Migrate(context.TODO(), f, NewLogger(t)); err == nil {
		t.Fatal("Expected error")
	}
}
