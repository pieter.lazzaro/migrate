package dataflex

import (
	"database/sql"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/pureharvest/df32"
)

func Test_CreateIntermediate(t *testing.T) {
	tests := []struct {
		name      string
		migration TableMigration
		expected  TableMigration
	}{
		{
			name: "intermediate file",
			migration: TableMigration{
				Action: "create",
				Table: df32.Table{
					Name: "TEST",
					IntermediateFile: df32.IntermediateFile{
						Schema: "test",
						Recnum: true,
					},
				},
				Fields: []FieldMigration{
					{Field: df32.Field{Name: "FIELD1", Type: "ascii", Size: df32.FieldSize{Length: 10}}},
				},
				Indexes: []IndexMigration{
					{Index: df32.Index{Name: "TEST001_PK", Clustered: true, PrimaryKey: true, Fields: []df32.IndexField{{Name: "FIELD1", Descending: true}}}},
				},
			},
			expected: TableMigration{
				Action: "create",
				Table: df32.Table{
					Name: "TEST",
					Fields: []df32.Field{
						{Number: 1, Name: "FIELD1", Type: "ascii", Size: df32.FieldSize{Length: 10}},
					},
					Indexes: []df32.Index{
						{Number: 0, Name: "TEST000", Fields: []df32.IndexField{{Name: "RECNUM"}}},
						{Number: 1, Name: "TEST001_PK", Clustered: true, PrimaryKey: true, Fields: []df32.IndexField{{Name: "FIELD1", Descending: true}}},
					},
					IntermediateFile: df32.IntermediateFile{
						DatabaseName:     "TEST",
						Schema:           "test",
						Recnum:           true,
						UseDummyZeroDate: true,
						Fields: []df32.Field{
							{Number: 1, Name: "FIELD1", Type: "ascii", Size: df32.FieldSize{Length: 10}},
						},
						Indexes: []df32.Index{
							{Number: 0, Name: "TEST000", Fields: []df32.IndexField{{Name: "RECNUM"}}},
							{Number: 1, Name: "TEST001_PK", Clustered: true, PrimaryKey: true, Fields: []df32.IndexField{{Name: "FIELD1", Descending: true}}},
						},
					},
				},
				Fields: []FieldMigration{
					{Field: df32.Field{Name: "FIELD1", Type: "ascii", Size: df32.FieldSize{Length: 10}}},
				},
				Indexes: []IndexMigration{
					{Index: df32.Index{Name: "TEST001_PK", Clustered: true, PrimaryKey: true, Fields: []df32.IndexField{{Name: "FIELD1", Descending: true}}}},
				},
			},
		},
		{
			name: "stkbal",
			migration: TableMigration{
				Action: "create",
				Table: df32.Table{
					Name: "STKBAL",
					IntermediateFile: df32.IntermediateFile{
						Schema: "test",
						Recnum: true,
					},
				},
				Fields: []FieldMigration{
					{Field: df32.Field{Name: "FIELD1", Type: "ascii", Size: df32.FieldSize{Length: 10}}},
				},
				Indexes: []IndexMigration{
					{Index: df32.Index{Name: "TEST000", Fields: []df32.IndexField{{Name: "RECNUM"}}}},
					{Index: df32.Index{Name: "TEST001", Fields: []df32.IndexField{{Name: "FIELD1"}}}},
				},
			},
			expected: TableMigration{
				Action: "create",
				Table: df32.Table{
					Name: "STKBAL",
					Fields: []df32.Field{
						{Number: 1, Name: "FIELD1", Type: "ascii", Size: df32.FieldSize{Length: 10}},
					},
					Indexes: []df32.Index{
						{Number: 0, Name: "TEST000", Fields: []df32.IndexField{{Name: "RECNUM"}}},
						{Number: 1, Name: "TEST001", Fields: []df32.IndexField{{Name: "FIELD1"}}},
					},
					IntermediateFile: df32.IntermediateFile{
						DatabaseName:     "STKBAL",
						Schema:           "test",
						Recnum:           true,
						UseDummyZeroDate: true,
						Fields: []df32.Field{
							{Number: 1, Name: "FIELD1", Type: "ascii", Size: df32.FieldSize{Length: 10}},
						},
						Indexes: []df32.Index{
							{Number: 0, Name: "TEST000", Fields: []df32.IndexField{{Name: "RECNUM"}}},
							{Number: 1, Name: "TEST001", Fields: []df32.IndexField{{Name: "FIELD1"}}},
						},
					},
				},
				Fields: []FieldMigration{
					{Field: df32.Field{Name: "FIELD1", Type: "ascii", Size: df32.FieldSize{Length: 10}}},
				},
				Indexes: []IndexMigration{
					{Index: df32.Index{Name: "TEST000", Fields: []df32.IndexField{{Name: "RECNUM"}}}},
					{Index: df32.Index{Name: "TEST001", Fields: []df32.IndexField{{Name: "FIELD1"}}}},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			env := &df32.Environment{}
			migration, err := prepareTableMigration(env, test.migration)

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expected, migration)
		})
	}
}

func Test_CreateTableMigrationScript(t *testing.T) {
	tests := []struct {
		name      string
		migration TableMigration
		expected  []string
	}{
		{
			name: "provided clustered index",
			migration: TableMigration{
				Action: "create",
				Table: df32.Table{
					Name: "TEST",
					IntermediateFile: df32.IntermediateFile{
						Schema: "test",
						Recnum: true,
					},
				},
				Fields: []FieldMigration{
					{Field: df32.Field{Name: "FIELD1", Type: "ascii", Size: df32.FieldSize{Length: 10}}},
				},
				Indexes: []IndexMigration{
					{Index: df32.Index{Name: "TEST001_PK", Clustered: true, PrimaryKey: true, Fields: []df32.IndexField{{Name: "FIELD1", Descending: true}}}},
				},
			},
			expected: []string{
				`CREATE TABLE [test].[TEST] (
    [RECNUM] INT IDENTITY (1, 1) NOT NULL,
    [FIELD1] NVARCHAR(10) CONSTRAINT [DF__TEST__FIELD1] DEFAULT ('') NOT NULL
    CONSTRAINT [TEST001_PK] PRIMARY KEY CLUSTERED ([FIELD1] DESC)
)`,
				"CREATE UNIQUE NONCLUSTERED INDEX [TEST000] ON [test].[TEST] ([RECNUM] ASC)",
			},
		},
		{
			name: "no primary key index",
			migration: TableMigration{
				Action: "create",
				Table: df32.Table{
					Name: "TEST",
					IntermediateFile: df32.IntermediateFile{
						Schema: "test",
						Recnum: true,
					},
				},
				Fields: []FieldMigration{
					{Field: df32.Field{Name: "FIELD1", Type: "ascii", Size: df32.FieldSize{Length: 10}}},
				},
				Indexes: []IndexMigration{
					{Index: df32.Index{Name: "TEST001", Fields: []df32.IndexField{{Name: "FIELD1", Descending: true}}}},
				},
			},
			expected: []string{
				`CREATE TABLE [test].[TEST] (
    [RECNUM] INT IDENTITY (1, 1) NOT NULL,
    [FIELD1] NVARCHAR(10) CONSTRAINT [DF__TEST__FIELD1] DEFAULT ('') NOT NULL
    CONSTRAINT [TEST000_PK] PRIMARY KEY CLUSTERED ([RECNUM] ASC)
)`,
				"CREATE UNIQUE NONCLUSTERED INDEX [TEST001] ON [test].[TEST] ([FIELD1] DESC)",
			},
		}, {
			name: "clustered, no primary key index",
			migration: TableMigration{
				Action: "create",
				Table: df32.Table{
					Name: "TEST",
					IntermediateFile: df32.IntermediateFile{
						Schema: "test",
						Recnum: true,
					},
				},
				Fields: []FieldMigration{
					{Field: df32.Field{Name: "FIELD1", Type: "ascii", Size: df32.FieldSize{Length: 10}}},
				},
				Indexes: []IndexMigration{
					{Index: df32.Index{Name: "TEST001", Clustered: true, Fields: []df32.IndexField{{Name: "FIELD1", Descending: true}}}},
				},
			},
			expected: []string{
				`CREATE TABLE [test].[TEST] (
    [RECNUM] INT IDENTITY (1, 1) NOT NULL,
    [FIELD1] NVARCHAR(10) CONSTRAINT [DF__TEST__FIELD1] DEFAULT ('') NOT NULL
    
)`,
				"CREATE UNIQUE NONCLUSTERED INDEX [TEST000] ON [test].[TEST] ([RECNUM] ASC)",
				"CREATE UNIQUE CLUSTERED INDEX [TEST001] ON [test].[TEST] ([FIELD1] DESC)",
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			env := &df32.Environment{}
			migration, err := prepareTableMigration(env, test.migration)

			if err != nil {
				t.Fatal(err)
			}

			statements, err := GenerateTableMigrationScript(migration, env.Driver.Generator())

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expected,
				statements,
			)
		})
	}
}

func Test_AlterTableMigrationScript(t *testing.T) {
	tests := []struct {
		name      string
		migration TableMigration
		expected  []string
	}{
		{
			name: "single column rename",
			migration: TableMigration{
				Action: "alter",
				Table: df32.Table{
					Name: "TEST",
					IntermediateFile: df32.IntermediateFile{
						Schema: "test",
						Recnum: true,
					},
				},
				Fields: []FieldMigration{
					{
						Action: "alter",
						Field: df32.Field{
							Name: "BEFORE",
						},
						To: df32.Field{
							Name: "AFTER",
						},
					},
				},
			},
			expected: []string{
				`EXEC sp_rename @objname=N'[test].[TEST].[BEFORE]', @newname=N'AFTER', @objtype=N'COLUMN';`,
			},
		},
		{
			name: "add columns",
			migration: TableMigration{
				Action: "alter",
				Table: df32.Table{
					Name: "TEST",
					IntermediateFile: df32.IntermediateFile{
						Schema: "test",
						Recnum: true,
					},
				},
				Fields: []FieldMigration{
					{
						Action: "create",
						Field: df32.Field{
							Name: "NEWFIELD",
							Type: "ascii",
							Size: df32.FieldSize{Length: 10},
						},
					},
				},
			},
			expected: []string{
				`ALTER TABLE [test].[TEST] ADD [NEWFIELD] NVARCHAR(10) CONSTRAINT [DF__TEST__NEWFIELD] DEFAULT ('') NOT NULL;`,
			},
		},
		{
			name: "drop columns",
			migration: TableMigration{
				Action: "alter",
				Table: df32.Table{
					Name: "TEST",
					IntermediateFile: df32.IntermediateFile{
						Schema: "test",
						Recnum: true,
					},
				},
				Fields: []FieldMigration{
					{
						Action: "drop",
						Field: df32.Field{
							Name: "OLDFIELD",
						},
					},
				},
			},
			expected: []string{
				`ALTER TABLE [test].[TEST] DROP [OLDFIELD];`,
			},
		},
		{
			name: "add, alter and drop columns",
			migration: TableMigration{
				Action: "alter",
				Table: df32.Table{
					Name: "TEST",
					IntermediateFile: df32.IntermediateFile{
						Schema: "test",
						Recnum: true,
					},
				},
				Fields: []FieldMigration{
					{
						Action: "create",
						Field: df32.Field{
							Name: "ADD_FIELD",
							Type: "ascii",
							Size: df32.FieldSize{Length: 10},
						},
					},
					{
						Action: "alter",
						Field: df32.Field{
							Name: "BEFORE",
						},
						To: df32.Field{
							Name: "AFTER",
						},
					},
					{
						Action: "drop",
						Field: df32.Field{
							Name: "DROP_FIELD",
						},
					},
				},
			},
			expected: []string{
				`ALTER TABLE [test].[TEST] ADD [ADD_FIELD] NVARCHAR(10) CONSTRAINT [DF__TEST__ADD_FIELD] DEFAULT ('') NOT NULL;`,
				`EXEC sp_rename @objname=N'[test].[TEST].[BEFORE]', @newname=N'AFTER', @objtype=N'COLUMN';`,
				`ALTER TABLE [test].[TEST] DROP [DROP_FIELD];`,
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			statements, err := GenerateTableMigrationScript(test.migration, df32.DefaultGenerator)

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expected,
				statements,
			)
		})
	}
}

func Test_ModifyFields(t *testing.T) {
	tests := []struct {
		name     string
		from     df32.Field
		to       df32.Field
		expected df32.Field
	}{
		{
			name: "rename",
			from: df32.Field{
				Name: "TEST",
				Type: "ascii",
				Size: df32.FieldSize{Length: 10},
			},
			to: df32.Field{
				Name: "TEST2",
			},
			expected: df32.Field{
				Name: "TEST2",
				Type: "ascii",
				Size: df32.FieldSize{Length: 10},
			},
		},
		{
			name: "change type",
			from: df32.Field{
				Name: "TEST",
				Type: "ascii",
				Size: df32.FieldSize{Length: 10},
			},
			to: df32.Field{
				Type: "number",
			},
			expected: df32.Field{
				Name: "TEST",
				Type: "number",
				Size: df32.FieldSize{Length: 10},
			},
		},
		{
			name: "change size",
			from: df32.Field{
				Name: "TEST",
				Type: "ascii",
				Size: df32.FieldSize{Length: 10},
			},
			to: df32.Field{
				Size: df32.FieldSize{Length: 20},
			},
			expected: df32.Field{
				Name: "TEST",
				Type: "ascii",
				Size: df32.FieldSize{Length: 20},
			},
		},
		{
			name: "change relation",
			from: df32.Field{
				Name: "TEST",
				Type: "ascii",
				Size: df32.FieldSize{Length: 10},
			},
			to: df32.Field{
				RelatesTo: df32.FieldRelation{
					File:  12,
					Field: 10,
				},
			},
			expected: df32.Field{
				Name: "TEST",
				Type: "ascii",
				Size: df32.FieldSize{Length: 10},
				RelatesTo: df32.FieldRelation{
					File:  12,
					Field: 10,
				},
			},
		},
		{
			name: "change multiple",
			from: df32.Field{
				Name: "TEST",
				Type: "ascii",
				Size: df32.FieldSize{Length: 10},
			},
			to: df32.Field{
				Name: "TEST2",
				Size: df32.FieldSize{Length: 20},
			},
			expected: df32.Field{
				Name: "TEST2",
				Type: "ascii",
				Size: df32.FieldSize{Length: 20},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			field := modifyField(test.from, test.to)

			assert.Equal(t, test.expected, field)
		})
	}
}

func runSQL(t *testing.T, connID dbConnection, query string) {

	db, err := sql.Open("mssql", connID.ConnectionString)

	if err != nil {
		t.Fatal(err)
	}

	if err := db.Ping(); err != nil {
		t.Fatal(err)
	}

	defer db.Close()

	if _, err := db.Exec(query); err != nil {
		t.Fatal(err)
	}

}

func Test_IndexValidation(t *testing.T) {
	tests := []struct {
		name string
		in   TableMigration
		out  []df32.Index
	}{
		{
			name: "with recnum index",
			in: TableMigration{
				Table: df32.Table{
					IntermediateFile: df32.IntermediateFile{
						Recnum: true,
					},
				},
				Indexes: []IndexMigration{
					{
						Index: df32.Index{
							Name: "test",
							Fields: []df32.IndexField{
								{Name: "RECNUM"},
							},
						},
					},
				},
			},
			out: []df32.Index{
				{
					Number: 0,
					Name:   "test",
					Fields: []df32.IndexField{
						{Name: "RECNUM"},
					},
				},
			},
		},
		{
			name: "without index",
			in: TableMigration{

				Table: df32.Table{
					Name: "TEST",
					IntermediateFile: df32.IntermediateFile{
						Recnum: true,
					},
				},
			},
			out: []df32.Index{
				{
					Name:       "TEST000_PK",
					Clustered:  true,
					PrimaryKey: true,
					Fields: []df32.IndexField{
						{Name: "RECNUM"},
					},
				},
			},
		},
		{
			name: "with index, but no recnum",
			in: TableMigration{

				Table: df32.Table{
					Name: "TEST",
					IntermediateFile: df32.IntermediateFile{
						Recnum: true,
					},
				},
				Indexes: []IndexMigration{
					{
						Index: df32.Index{
							Name: "TEST001",
							Fields: []df32.IndexField{
								{Name: "FIELD"},
							},
						},
					},
				},
			},
			out: []df32.Index{
				{
					Name:       "TEST000_PK",
					Clustered:  true,
					PrimaryKey: true,
					Fields: []df32.IndexField{
						{Name: "RECNUM"},
					},
				},
				{
					Number: 1,
					Name:   "TEST001",
					Fields: []df32.IndexField{
						{Name: "FIELD"},
					},
				},
			},
		},
		{
			name: "with clusted index, no recnum, no pk",
			in: TableMigration{

				Table: df32.Table{
					Name: "TEST",
					IntermediateFile: df32.IntermediateFile{
						Recnum: true,
					},
				},
				Indexes: []IndexMigration{
					{
						Index: df32.Index{
							Name:      "TEST001",
							Clustered: true,
							Fields: []df32.IndexField{
								{Name: "FIELD"},
							},
						},
					},
				},
			},
			out: []df32.Index{
				{
					Name: "TEST000",
					Fields: []df32.IndexField{
						{Name: "RECNUM"},
					},
				},
				{
					Number:    1,
					Name:      "TEST001",
					Clustered: true,
					Fields: []df32.IndexField{
						{Name: "FIELD"},
					},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			indexes := createMissingIndexes(test.in)

			assert.Equal(t, test.out, indexes)
		})
	}
}

// func Test_TableLoad(t *testing.T) {
// 	host := os.Getenv("MSSQL_PORT_1433_TCP_ADDR")
// 	port := os.Getenv("MSSQL_PORT_1433_TCP_PORT")
// 	user := os.Getenv("MSSQL_USER_PWD")

// 	drv := mssqldrv{
// 		connections: []dbConnection{dbConnection{
// 			Name:             "POSONA",
// 			ConnectionString: fmt.Sprintf("server=%s;port=%s;Database=pureharvest_test;%s", host, port, user),
// 		},
// 		},
// 	}

// 	runSQL(t, drv.connections[0], `
// CREATE TABLE [dataflex].[TEST] (
//     [RECNUM] INT IDENTITY (1, 1) NOT NULL,
//     [NUMBER] NVARCHAR (10) CONSTRAINT [DF__TEST__NUMBER] DEFAULT ('') NOT NULL
//     CONSTRAINT [TEST000_PK] PRIMARY KEY CLUSTERED ([RECNUM] ASC)
// )
// `)

// 	defer func() {
// 		runSQL(t, drv.connections[0], `DROP TABLE [dataflex].[TEST]`)
// 	}()

// 	intFile := []byte(`
// DRIVER_NAME MSSQLDRV
// SERVER_NAME CONNID=POSONA;
// DATABASE_NAME TEST
// SCHEMA_NAME dataflex

// RECNUM_TABLE YES
// PRIMARY_INDEX 0
// GENERATE_RECORD_ID_METHOD IDENTITY_COLUMN
// TABLE_CHARACTER_FORMAT ANSI
// USE_DUMMY_ZERO_DATE YES

// FIELD_NUMBER 1
// FIELD_INDEX 1

// INDEX_NUMBER 0
// INDEX_NAME TEST000_PK

// `)
// 	table := DataflexTable{}

// 	if err := table.LoadIntermediateFile(bytes.NewReader(intFile)); err != nil {
// 		t.Fatal(err)
// 	}

// 	if err := drv.LoadTableStructure(&table); err != nil {
// 		t.Fatal(err)
// 	}

// 	expected := DataflexTable{
// 		Name:               "TEST",
// 		Schema:             "dataflex",
// 		Server:             "CONNID=POSONA;",
// 		Recnum:             true,
// 		PrimaryIndexNumber: 0,
// 		RecordIdentity:     "IDENTITY_COLUMN",
// 		Format:             "ANSI",
// 		UseDummyZeroDate:   true,
// 		Fields: []Field{
// 			Field{Number: 1, Name: "NUMBER", Type: "nvarchar", Index: 1, Size: df32.FieldSize{Precision: 10}},
// 		},
// 		Indexes: []Index{
// 			Index{Name: "TEST000_PK", Clustered: true, Number: 0, Fields: []IndexField{IndexField{Name: "RECNUM", Descending: false, Number: 1}}},
// 		},
// 	}
// 	assert.Equal(t, expected, table)
// }
