package dataflex

import (
	"strings"

	"github.com/pkg/errors"

	"gitlab.com/pureharvest/df32"
)

func GenerateTableMigrationScript(migration TableMigration, g df32.SQLGenerator) ([]string, error) {

	var statements []string

	tableName := migration.IntermediateFile.DatabaseName

	if tableName == "" {
		tableName = migration.Name
	}

	schema := migration.IntermediateFile.Schema

	switch strings.ToLower(migration.Action) {
	case "create":
		table := migration.Table

		statements = append(statements, g.CreateTable(table))

		for _, index := range table.Indexes {
			if !index.PrimaryKey {
				statements = append(statements, g.CreateIndex(schema, tableName, index.Name, index.Clustered, index.Fields))
			}

		}

	case "alter":
		fields := migration.Fields

		for _, field := range fields {
			switch strings.ToLower(field.Action) {
			case "alter":
				if isRename(field) {
					statements = append(statements, g.RenameField(schema, tableName, field.Name, field.To.Name))
				}

				if isTypeChange(field) {
					statements = append(statements, g.AlterField(schema, tableName, field.Field, field.To))
				}
			case "create":
				statements = append(statements, g.AddField(schema, tableName, field.Field))
			case "drop":
				statements = append(statements, g.DropField(schema, tableName, field.Name))
			}

		}
	}

	return statements, nil
}

func isRename(field FieldMigration) bool {
	return field.To.Name != field.Name
}

func isTypeChange(field FieldMigration) bool {
	return field.To.Type != "" || (field.To.Size.Length != 0 || field.To.Size.Precision != 0)
}

func GenerateIntermediateFile(migration TableMigration, drv *df32.MSSQLDRV) (df32.IntermediateFile, error) {
	return migration.Table.IntermediateFile, nil
}

func prepateTableMigrations(env *df32.Environment, migrations []TableMigration) ([]TableMigration, error) {
	prepared := make([]TableMigration, 0, len(migrations))

	for _, migration := range migrations {
		m, err := prepareTableMigration(env, migration)
		if err != nil {
			return nil, err
		}

		prepared = append(prepared, m)
	}

	return prepared, nil
}

func prepareCreateMigration(migration TableMigration, defaultConnectionString string) TableMigration {

	if migration.Server == "" {
		migration.Server = defaultConnectionString
	}

	if migration.System {
		migration.Recnum = false
		migration.RecordIdentity = ""
	}

	migration.UseDummyZeroDate = true

	for i, field := range migration.Fields {
		field.Field.Number = i + 1
		migration.Table.Fields = append(migration.Table.Fields, field.Field)
	}

	migration.IntermediateFile.Fields = migration.Table.Fields

	migration.Table.Indexes = createMissingIndexes(migration)
	migration.IntermediateFile.Indexes = migration.Table.Indexes

	return migration
}

func prepareTableMigration(env *df32.Environment, migration TableMigration) (TableMigration, error) {

	if migration.DatabaseName == "" {
		migration.DatabaseName = migration.Name
	}

	if strings.EqualFold(migration.Action, "create") {
		migration = prepareCreateMigration(migration, env.Driver.DefaultConnectionString())
	} else if strings.EqualFold(migration.Action, "alter") {
		var entry df32.FilelistEntry
		var err error

		if migration.Number != 0 {
			entry, err = env.Filelist.Get(int(migration.Number))

			if err != nil {
				return migration, errors.Wrap(err, "could not load table")
			}

		} else if migration.Name != "" {
			entry, err = env.Filelist.GetByName(migration.Name)

			if err != nil {
				return migration, errors.Wrap(err, "could not load table")
			}
		}

		table, err := env.Driver.LoadTable(entry)

		if err != nil {
			return migration, errors.Wrap(err, "could not load table")
		}

		// TODO: Validate other table properties if they are set?

		migration.Table, err = alterTable(table, migration)
	}

	return migration, nil
}

// findField returns the index of field in the fields or -1 if not found
func findField(fields []df32.Field, field df32.Field) int {
	for i, f := range fields {
		if f.Name == field.Name {
			return i
		}
	}

	return -1
}

// findIndex returns the index of index in the indexes or -1 if not found
func findIndex(indexes []df32.Index, index df32.Index) int {
	for i, ind := range indexes {
		if ind.Name == index.Name {
			return i
		}
	}

	return -1
}

func modifyField(from df32.Field, to df32.Field) df32.Field {
	if to.Name != "" {
		from.Name = to.Name
	}

	if to.Type != "" {
		from.Type = to.Type
	}

	if to.Size.Length != 0 {
		from.Size = to.Size
	}

	if to.Index != 0 {
		from.Index = to.Index
	}

	if to.RelatesTo.Field != 0 {
		from.RelatesTo.Field = to.RelatesTo.Field
	}

	if to.RelatesTo.File != 0 {
		from.RelatesTo.File = to.RelatesTo.File
	}

	return from
}

func alterTable(table df32.Table, migration TableMigration) (df32.Table, error) {

	for _, m := range migration.Fields {

		switch strings.ToLower(m.Action) {
		case "create":
			table.Fields = append(table.Fields, m.Field)
		case "alter":
			i := findField(table.Fields, m.Field)

			if i == -1 {
				return table, errors.Errorf("could not find field %s", m.Name)
			}

			table.Fields[i] = modifyField(table.Fields[i], m.To)

		case "delete":
			i := findField(table.Fields, m.Field)

			if i == -1 {
				return table, errors.Errorf("could not find field %s", m.Name)
			}

			table.Fields = append(table.Fields[:i], table.Fields[i+1:]...)
		}
	}

	for _, m := range migration.Indexes {
		switch strings.ToLower(m.Action) {
		case "create":
			table.Indexes = append(table.Indexes, m.Index)

		case "delete":
			i := findIndex(table.Indexes, m.Index)

			if i == -1 {
				return table, errors.Errorf("could not find index %s", m.Name)
			}

			table.Indexes = append(table.Indexes[:i], table.Indexes[i+1:]...)
		}
	}

	return table, nil
}

func createMissingIndexes(migration TableMigration) []df32.Index {

	var hasRecnumIndex, hasClustered, hasPrimaryKey bool
	var indexes []df32.Index

	tableName := migration.DatabaseName

	if tableName == "" {
		tableName = migration.Name
	}

	for _, index := range migration.Indexes {
		if strings.EqualFold(index.Fields[0].Name, "RECNUM") {
			hasRecnumIndex = true
		}

		if index.Clustered {
			hasClustered = true
		}

		if index.PrimaryKey {
			hasPrimaryKey = true
		}

		indexes = append(indexes, index.Index)
	}

	if migration.Recnum && !hasRecnumIndex && !migration.System {
		recnumIndex := df32.Index{
			Name:      tableName + "000",
			Clustered: !hasClustered,
			Fields: []df32.IndexField{
				{Name: "RECNUM"},
			},
		}

		if !hasPrimaryKey && !hasClustered {
			recnumIndex.Name = recnumIndex.Name + "_PK"
			recnumIndex.PrimaryKey = true
		}

		indexes = append([]df32.Index{recnumIndex}, indexes...)
	}

	for i := range indexes {
		indexes[i].Number = i
	}

	return indexes
}
