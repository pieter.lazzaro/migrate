package dataflex

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/pureharvest/df32"

	"strings"

	"path/filepath"

	"github.com/ghodss/yaml"
	"github.com/pkg/errors"
	"gitlab.com/pieter.lazzaro/migrate/driver"
	"gitlab.com/pieter.lazzaro/migrate/file"
	"gitlab.com/pieter.lazzaro/migrate/migrate/direction"
)

type dbConnection struct {
	Name             string
	ConnectionString string
	Silent           bool
}

type versionInfo uint64

const tableName = "schema_migrations"

type Driver struct {
	env *df32.Environment

	db *sql.DB
}

// Driver will connect with dataflex://{{DFPATH}}

func (driver *Driver) Initialize(url string) error {
	var err error

	pathSeg := strings.TrimPrefix(url, "dataflex://")

	env, err := df32.NewEnvironment(filepath.SplitList(pathSeg))

	if err != nil {
		return errors.Wrap(err, "could not initialize dataflex environment")
	}

	driver.env = env

	connections := env.Driver.Connections()

	if len(connections) != 1 {
		log.Println(connections)
		return errors.New("migrations are only supported with a single connection")
	}

	connectionString := connections[0]

	driver.db, err = sql.Open("mssql", connectionString)

	if err != nil {
		return errors.Wrap(err, "could not connect to database")
	}

	if err := driver.db.Ping(); err != nil {
		return errors.Wrap(err, "could not connect to database")
	}

	if err := driver.ensureVersionTableExists(); err != nil {
		return err
	}

	env.Driver.SetDB(driver.db)

	return nil

}

func (driver *Driver) ensureVersionTableExists() error {
	if _, err := driver.db.Exec(fmt.Sprintf(`
	if not exists (
		select 1 from sys.tables t join sys.schemas s on (t.schema_id = s.schema_id) where s.name = 'dbo' and t.name = '%s') 
	create table [dbo].[%s] (version int not null primary key);`, tableName, tableName)); err != nil {
		return err
	}
	return nil
}

func (driver *Driver) Close() error {

	driver.env.Close()

	return nil
}

func (driver *Driver) FilenameExtension() string {
	return "yml"
}

func (driver *Driver) Migrate(ctx context.Context, f file.File, log driver.Logger) error {

	if err := f.ReadContent(); err != nil {
		return err
	}

	migration := Migration{}

	if err := yaml.Unmarshal(f.Content, &migration); err != nil {
		return err
	}

	tx, err := driver.db.Begin()

	if err != nil {
		return errors.Wrap(err, "could not run migration")
	}

	if f.Direction == direction.Up {
		if _, err := tx.Exec("INSERT INTO [dbo].["+tableName+"] (version) VALUES ($1)", f.Version); err != nil {
			return driver.rollBackOnError(err, tx)
		}
	} else if f.Direction == direction.Down {
		if _, err := tx.Exec("DELETE FROM [dbo].["+tableName+"] WHERE version=$1", f.Version); err != nil {
			return driver.rollBackOnError(err, tx)
		}
	}

	if len(migration.Tables) > 0 {

		migrations, err := prepateTableMigrations(driver.env, migration.Tables)

		if err != nil {
			return err
		}

		statements, err := driver.generateSQLStatements(migrations, log)

		if err != nil {
			return err
		}

		for _, statement := range statements {
			_, err := tx.Exec(statement)
			if err != nil {

				return driver.rollBackOnError(err, tx)
			}
		}

		for _, migration := range migrations {
			if err := driver.env.Driver.WriteIntermediateFile(migration.Name, migration.Table.IntermediateFile); err != nil {
				return driver.rollBackOnError(err, tx)
			}

			if err := driver.env.WriteFileDefinition(migration.Table); err != nil {
				return driver.rollBackOnError(err, tx)
			}

			entry := df32.FilelistEntry{
				Driver:       "MSSQLDRV",
				DisplayName:  migration.DisplayName,
				DataflexName: migration.Name,
				Name:         migration.Name,
				Number:       migration.Number,
			}

			if err := driver.env.Filelist.Set(entry); err != nil {
				return driver.rollBackOnError(err, tx)
			}
		}
	} else if migration.SQL != "" {

		_, err = tx.Exec(migration.SQL)

		if err != nil {
			return driver.rollBackOnError(err, tx)
		}

	} else if len(migration.Programs) != 0 {

		for _, program := range migration.Programs {

			log.Println("Running ", program)
			cmd := driver.env.Command(program)

			output, err := cmd.CombinedOutput()

			log.Print(output)

			if err != nil {
				return driver.rollBackOnError(err, tx)
			}

		}

	}

	if err := tx.Commit(); err != nil {
		return driver.rollBackOnError(err, tx)
	}

	log.Println("Finished migration...")
	return driver.env.WriteFilelist()

}

func (driver *Driver) generateSQLStatements(migrations []TableMigration, log driver.Logger) ([]string, error) {

	var statements []string

	for _, migration := range migrations {
		var err error

		log.Printf("Building SQL migrations for %s", migration.Name)

		s, err := GenerateTableMigrationScript(migration, driver.env.Driver.Generator())

		if err != nil {
			return nil, errors.Wrap(err, "could not generate a migration script")
		}

		statements = append(statements, s...)
	}

	return statements, nil
}

func (driver *Driver) rollBackOnError(err error, tx *sql.Tx) error {
	if err2 := tx.Rollback(); err2 != nil {
		return errors.Wrap(err, err2.Error())
	}
	return err
}
func (driver *Driver) Version() (uint64, error) {
	var version uint64
	err := driver.db.QueryRow("SELECT TOP 1 version FROM [dbo].[" + tableName + "] ORDER BY version DESC").Scan(&version)
	switch {
	case err == sql.ErrNoRows:
		return 0, nil
	case err != nil:
		return 0, err
	default:
		return version, nil
	}
}

func (driver *Driver) Baseline(version uint64) error {

	tx, err := driver.db.Begin()

	if err != nil {
		return errors.Wrap(err, "could not set baseline")
	}

	if _, err := tx.Exec("INSERT INTO [dbo].["+tableName+"] (version) VALUES ($1)", version); err != nil {
		return driver.rollBackOnError(err, tx)
	}

	if err := tx.Commit(); err != nil {
		return driver.rollBackOnError(err, tx)
	}

	return nil
}

func init() {
	driver.RegisterDriver("dataflex", &Driver{})
}
