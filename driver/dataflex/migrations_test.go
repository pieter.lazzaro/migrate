package dataflex

import (
	"testing"

	"gitlab.com/pureharvest/df32"

	"github.com/ghodss/yaml"
	"github.com/stretchr/testify/assert"
)

func Test_YAMLDefaultsTableMigration(t *testing.T) {
	tests := []struct {
		name     string
		input    []byte
		expected TableMigration
	}{
		{
			name: "all default",
			input: []byte(`
name: BLINE
file_num: 85
`),
			expected: TableMigration{
				Table: df32.Table{
					Name:   "BLINE",
					Number: 85,
					IntermediateFile: df32.IntermediateFile{
						Recnum:           true,
						UseDummyZeroDate: true,
						RecordIdentity:   "IDENTITY_COLUMN",
						Format:           "ANSI",
					},
				},
			},
		},
		{
			name: "",
			input: []byte(`
action: create
name: BLINE
file_num: 85
recnum: true
schema: test
display_name: Barcode Scanning Line Item File
fields:
- name: NUMBER
  type: ascii
  size: 10
  use_index: 1
- name: DATE
  type: date
  size: 6
indexes:
- name: BLINE001_PK
  clustered: true
  fields:
  - name: BARCODE
    descending: NO
    uppercase: YES
`),
			expected: TableMigration{
				Action: "create",
				Table: df32.Table{
					Number:      85,
					Name:        "BLINE",
					DisplayName: "Barcode Scanning Line Item File",
					IntermediateFile: df32.IntermediateFile{
						Schema:           "test",
						Recnum:           true,
						UseDummyZeroDate: true,
						RecordIdentity:   "IDENTITY_COLUMN",
						Format:           "ANSI",
					},
				},
				Fields: []FieldMigration{
					{
						Field: df32.Field{
							Name:  "NUMBER",
							Type:  "ascii",
							Size:  df32.FieldSize{Length: 10},
							Index: 1,
						},
					},
					{
						Field: df32.Field{
							Name: "DATE",
							Type: "date",
							Size: df32.FieldSize{Length: 6},
						},
					},
				},
				Indexes: []IndexMigration{
					{
						Index: df32.Index{
							Name:      "BLINE001_PK",
							Clustered: true,
							Fields: []df32.IndexField{
								{Name: "BARCODE", Uppercase: true},
							},
						},
					},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			drv, err := df32.NewMSSQLDRV(nil)

			if err != nil {
				t.Fatal(err)
			}

			table := TableMigration{
				Table: df32.Table{
					IntermediateFile: drv.DefaultIntermediate(),
				},
			}

			if err := yaml.Unmarshal(test.input, &table); err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expected, table)
		})
	}
}

func Test_YAMLSQLMigration(t *testing.T) {
	tests := []struct {
		name     string
		input    []byte
		expected Migration
	}{
		{
			name: "all default",
			input: []byte(`
---
sql: |
    Test multiple lines
    asdasd
    aaa

`),
			expected: Migration{
				SQL: `Test multiple lines
asdasd
aaa
`,
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			migration := Migration{}

			err := yaml.Unmarshal(test.input, &migration)
			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expected, migration)
		})
	}
}
