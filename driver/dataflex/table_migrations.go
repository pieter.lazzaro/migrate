package dataflex

import "gitlab.com/pureharvest/df32"

type TableMigration struct {
	df32.Table `json:",inline"`

	Action string

	Fields  []FieldMigration
	Indexes []IndexMigration
}

type FieldMigration struct {
	TableName string
	Action    string
	To        df32.Field
	df32.Field
}

type IndexMigration struct {
	df32.Index `json:",inline"`
	Action     string
}
