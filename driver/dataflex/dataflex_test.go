package dataflex

import (
	"context"
	"io/ioutil"
	"math/rand"
	"os"
	"path"
	"path/filepath"
	"testing"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/pureharvest/df32"

	"fmt"

	"database/sql"

	testdriver "gitlab.com/pieter.lazzaro/migrate/driver/test"
	"gitlab.com/pieter.lazzaro/migrate/file"
	"gitlab.com/pieter.lazzaro/migrate/migrate/direction"
)

func createDB(t *testing.T, db *sql.DB, name string) {

	if _, err := db.Exec(fmt.Sprintf("CREATE DATABASE [%s]", name)); err != nil {
		t.Fatal(err)
	}

	if _, err := db.Exec(fmt.Sprintf("USE [%s]; EXEC (N'CREATE SCHEMA [test]'); USE [master]", name)); err != nil {
		t.Fatal(err)
	}

}

func dropDB(t *testing.T, db *sql.DB, name string) {
	if _, err := db.Exec(fmt.Sprintf("ALTER DATABASE [%s] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;", name)); err != nil {
		t.Fatal(err)
	}

	if _, err := db.Exec(fmt.Sprintf("DROP DATABASE [%s]", name)); err != nil {
		t.Fatal(err)
	}
}

func randomDBName(prefix string) string {

	rand.Seed(time.Now().UTC().UnixNano())
	const chars = "abcdefghijklmnopqrstuvwxyz0123456789"
	result := make([]byte, 10)
	for i := 0; i < 10; i++ {
		result[i] = chars[rand.Intn(len(chars))]
	}
	return prefix + string(result)

}

func createInstance(t *testing.T) (tmpdir string, deferFn func()) {

	host := os.Getenv("MSSQL_PORT_1433_TCP_ADDR")
	user := os.Getenv("MSSQL_USER_PWD")

	if host == "" {
		host = "127.0.0.1"
	}

	databaseName := randomDBName("df")

	connectionString := fmt.Sprintf("server=%s;%s", host, user)

	db, err := sql.Open("mssql", df32.ConvertDataflexConnectionString(connectionString)+";log=63;")

	if err != nil {
		t.Fatal(err)
	}

	createDB(t, db, databaseName)

	tmpdir, err = ioutil.TempDir(os.TempDir(), "df")

	if err != nil {
		t.Fatal(err)
	}

	intfile := fmt.Sprintf(`
DFConnectionId POSONA, %s;database=%s, 1
    `, connectionString, databaseName)

	t.Log(intfile)
	if err := ioutil.WriteFile(path.Join(tmpdir, "mssqldrv.int"), []byte(intfile), 0666); err != nil {
		t.Fatal(err)
	}

	return tmpdir, func() {
		dropDB(t, db, databaseName)
		db.Close()
		os.RemoveAll(tmpdir)
	}

}

func Test_CreateMigration(t *testing.T) {

	tmpdir, fn := createInstance(t)

	t.Log(tmpdir)
	defer fn()

	url := fmt.Sprintf("dataflex://%s", tmpdir)

	d := Driver{}

	err := d.Initialize(url)

	if err != nil {
		t.Fatal(err)
	}

	defer d.Close()

	t.Logf("%#v", d.env)

	files := []file.File{
		{
			Path:      "/foobar",
			FileName:  "001_foobar.up.yml",
			Version:   1,
			Name:      "foobar",
			Direction: direction.Up,
			Content: []byte(`
tables:
- name: BLINE
  action: create
  file_num: 85
  recnum: true
  schema: test
  display_name: Barcode Scanning Line Item File
  fields:
    - name: NUMBER
      type: ascii
      size: "10"
      use_index: 1

    - name: CREDITOR
      type: ascii
      size: "15"
      use_index: 1

    - name: STOCK
      type: ascii
      size: "20"

    - name: BARCODE
      type: ascii
      size: "20"
      use_index: 1
  indexes:
    - name: BLINE001_PK
      clustered: true
      fields:
        - name: BARCODE
          descending: NO
          uppercase: YES
      `),
		},
	}

	if err := d.Migrate(context.TODO(), files[0], testdriver.NewLogger(t)); err != nil {
		t.Fatal(err)
	}

	var exists int
	row := d.db.QueryRow(`SELECT 1 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'test' 
                 AND  TABLE_NAME = 'BLINE'`)

	if err := row.Scan(&exists); err != nil {
		t.Fatal("expected BLINE to exist", err)
	}

	if _, err := os.Stat(filepath.Join(tmpdir, "BLINE.int")); os.IsNotExist(err) {
		t.Fatal("expected BLINE int file to be created")
	}

	if _, err := os.Stat(filepath.Join(tmpdir, "BLINE.fd")); os.IsNotExist(err) {
		t.Fatal("expected BLINE file definition file to be created")
	}
}

func Test_AlterMigration(t *testing.T) {

	tmpdir, _ := createInstance(t)

	t.Log(tmpdir)
	// defer fn()

	url := fmt.Sprintf("dataflex://%s", tmpdir)

	d := Driver{}

	err := d.Initialize(url)

	if err != nil {
		t.Fatal(err)
	}

	defer d.Close()

	d.env.Filelist.Set(df32.FilelistEntry{
		Number:       85,
		Driver:       "MSSQLDRV",
		Name:         "BLINE",
		DataflexName: "BLINE",
	})

	_, err = d.db.Exec(`CREATE TABLE [test].[BLINE] (
    [RECNUM] INT IDENTITY (1, 1) NOT NULL,
    [NUMBER] NVARCHAR (10) CONSTRAINT [DF__TEST__NUMBER] DEFAULT ('') NOT NULL
    CONSTRAINT [TEST000_PK] PRIMARY KEY CLUSTERED ([RECNUM] ASC)
);`)

	if err != nil {
		t.Fatal(err)
	}

	if err := ioutil.WriteFile(filepath.Join(tmpdir, "BLINE.int"), []byte(`
DRIVER_NAME MSSQLDRV
SERVER_NAME DFCONNID=POSONA;
DATABASE_NAME BLINE
SCHEMA_NAME test

RECNUM_TABLE YES
PRIMARY_INDEX 0
GENERATE_RECORD_ID_METHOD IDENTITY_COLUMN
TABLE_CHARACTER_FORMAT ANSI
USE_DUMMY_ZERO_DATE YES

INDEX_NUMBER 0
INDEX_NAME TEST000_PK
`), 0666); err != nil {
		t.Fatal(err)
	}

	files := []file.File{
		{
			Path:      "/foobar",
			FileName:  "001_foobar.up.yml",
			Version:   1,
			Name:      "foobar",
			Direction: direction.Up,
			Content: []byte(`
tables:
- name: BLINE
  action: alter
  fields:
    - name: NUMBER
      to:
        name: NUMBER2
`),
		},
	}

	if err := d.Migrate(context.TODO(), files[0], testdriver.NewLogger(t)); err != nil {

		t.Fatalf("%#v", errors.Cause(err))
	}

}

// func Test_Migrations(t *testing.T) {

// 	tests := []struct {
// 		name string
// 	}{
// 		{name: "alter"},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {

// 			env, fn := df32.CreateTestEnvironment(t)

// 			defer fn()
// 			t.Log(env.DFPATH())
// 			url := fmt.Sprintf("dataflex://%s", strings.Join(env.DFPATH(), string(filepath.ListSeparator)))

// 			d := Driver{}

// 			err := d.Initialize(url)

// 			if err != nil {
// 				t.Fatal(err)
// 			}

// 			defer d.Close()

// 			files, err := file.ReadMigrationFiles(filepath.Join("./test-fixtures", test.name), file.FilenameRegex(d.FilenameExtension()))

// 			if err != nil {
// 				t.Fatal(err)
// 			}

// 			for _, file := range files {

// 				if err := d.Migrate(context.TODO(), *file.UpFile, testdriver.NewLogger(t)); err != nil {
// 					t.Fatal(err)
// 				}
// 			}

// 		})
// 	}

// }
