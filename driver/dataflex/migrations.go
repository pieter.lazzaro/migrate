package dataflex

type Migration struct {
	Tables   []TableMigration
	SQL      string
	Programs []string
}
