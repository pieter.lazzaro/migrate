package mysql

import (
	"context"
	"database/sql"
	"os"
	"strings"
	"testing"

	testdriver "gitlab.com/pieter.lazzaro/migrate/driver/test"
	"gitlab.com/pieter.lazzaro/migrate/file"
	"gitlab.com/pieter.lazzaro/migrate/migrate/direction"
)

// TestMigrate runs some additional tests on Migrate().
// Basic testing is already done in migrate/migrate_test.go
func TestMigrate(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}
	host := os.Getenv("MYSQL_PORT_3306_TCP_ADDR")
	port := os.Getenv("MYSQL_PORT_3306_TCP_PORT")
	driverUrl := "mysql://root@tcp(" + host + ":" + port + ")/migratetest"

	// prepare clean database
	connection, err := sql.Open("mysql", strings.SplitN(driverUrl, "mysql://", 2)[1])
	if err != nil {
		t.Fatal(err)
	}

	if _, err := connection.Exec(`DROP TABLE IF EXISTS yolo, yolo1, ` + tableName); err != nil {
		t.Fatal(err)
	}

	d := &Driver{}
	if err := d.Initialize(driverUrl); err != nil {
		t.Fatal(err)
	}

	files := []file.File{
		{
			Path:      "/foobar",
			FileName:  "001_foobar.up.sql",
			Version:   1,
			Name:      "foobar",
			Direction: direction.Up,
			Content: []byte(`
        CREATE TABLE yolo (
          id int(11) not null primary key auto_increment
        );

				CREATE TABLE yolo1 (
				  id int(11) not null primary key auto_increment
				);
      `),
		},
		{
			Path:      "/foobar",
			FileName:  "002_foobar.down.sql",
			Version:   1,
			Name:      "foobar",
			Direction: direction.Down,
			Content: []byte(`
        DROP TABLE yolo;
      `),
		},
		{
			Path:      "/foobar",
			FileName:  "002_foobar.up.sql",
			Version:   2,
			Name:      "foobar",
			Direction: direction.Up,
			Content: []byte(`

      	// a comment
				CREATE TABLE error (
          id THIS WILL CAUSE AN ERROR
        );
      `),
		},
	}

	err = d.Migrate(context.Background(), files[0], testdriver.NewLogger(t))

	if err != nil {
		t.Fatal(err)
	}

	err = d.Migrate(context.Background(), files[1], testdriver.NewLogger(t))

	if err != nil {
		t.Fatal(err)
	}

	err = d.Migrate(context.Background(), files[2], testdriver.NewLogger(t))

	if err == nil {
		t.Error("Expected test case to fail")
	}
	if err := d.Close(); err != nil {
		t.Fatal(err)
	}
}
