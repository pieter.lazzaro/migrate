// Package mysql implements the Driver interface.
package mysql

import (
	"bufio"
	"bytes"
	"context"
	"database/sql"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"github.com/pkg/errors"

	"github.com/go-sql-driver/mysql"
	"gitlab.com/pieter.lazzaro/migrate/driver"
	"gitlab.com/pieter.lazzaro/migrate/file"
	"gitlab.com/pieter.lazzaro/migrate/migrate/direction"
)

type Driver struct {
	db *sql.DB
}

const tableName = "schema_migrations"

func (driver *Driver) Initialize(url string) error {
	urlWithoutScheme := strings.SplitN(url, "mysql://", 2)
	if len(urlWithoutScheme) != 2 {
		return errors.New("invalid mysql:// scheme")
	}

	db, err := sql.Open("mysql", urlWithoutScheme[1])
	if err != nil {
		return err
	}
	if err := db.Ping(); err != nil {
		return err
	}
	driver.db = db

	if err := driver.ensureVersionTableExists(); err != nil {
		return err
	}
	return nil
}

func (driver *Driver) Close() error {
	if err := driver.db.Close(); err != nil {
		return err
	}
	return nil
}

func (driver *Driver) ensureVersionTableExists() error {
	_, err := driver.db.Exec("CREATE TABLE IF NOT EXISTS " + tableName + " (version int not null primary key);")

	if _, isWarn := err.(mysql.MySQLWarnings); err != nil && !isWarn {
		return err
	}

	return nil
}

func (driver *Driver) FilenameExtension() string {
	return "sql"
}

func (driver *Driver) rollBackOnError(err error, tx *sql.Tx) error {
	if err2 := tx.Rollback(); err2 != nil {
		return errors.Wrap(err, err2.Error())
	}
	return err
}

func (driver *Driver) Migrate(ctx context.Context, f file.File, log driver.Logger) error {

	// http://go-database-sql.org/modifying.html, Working with Transactions
	// You should not mingle the use of transaction-related functions such as Begin() and Commit() with SQL statements such as BEGIN and COMMIT in your SQL code.
	tx, err := driver.db.Begin()
	if err != nil {
		return err
	}

	if f.Direction == direction.Up {
		if _, err := tx.Exec("INSERT INTO "+tableName+" (version) VALUES (?)", f.Version); err != nil {
			return driver.rollBackOnError(err, tx)
		}
	} else if f.Direction == direction.Down {
		if _, err := tx.Exec("DELETE FROM "+tableName+" WHERE version = ?", f.Version); err != nil {
			return driver.rollBackOnError(err, tx)
		}
	}

	if err := f.ReadContent(); err != nil {
		return err
	}

	// TODO this is not good! unfortunately there is no mysql driver that
	// supports multiple statements per query.
	sqlStmts := bytes.Split(f.Content, []byte(";"))

	for _, sqlStmt := range sqlStmts {
		sqlStmt = bytes.TrimSpace(sqlStmt)
		if len(sqlStmt) > 0 {
			if _, err := tx.Exec(string(sqlStmt)); err != nil {
				mysqlErr, isErr := err.(*mysql.MySQLError)

				if isErr {
					re, err := regexp.Compile(`at line ([0-9]+)$`)
					if err != nil {
						return driver.rollBackOnError(err, tx)
					}

					var lineNo int
					lineNoRe := re.FindStringSubmatch(mysqlErr.Message)
					if len(lineNoRe) == 2 {
						lineNo, err = strconv.Atoi(lineNoRe[1])
					}
					if err == nil {

						// get white-space offset
						// TODO this is broken, because we use sqlStmt instead of f.Content
						wsLineOffset := 0
						b := bufio.NewReader(bytes.NewBuffer(sqlStmt))
						for {
							line, _, err := b.ReadLine()
							if err != nil {
								break
							}
							if bytes.TrimSpace(line) == nil {
								wsLineOffset += 1
							} else {
								break
							}
						}

						message := mysqlErr.Error()
						message = re.ReplaceAllString(message, fmt.Sprintf("at line %v", lineNo+wsLineOffset))

						errorPart := file.LinesBeforeAndAfter(sqlStmt, lineNo, 5, 5, true)
						err = errors.New(fmt.Sprintf("%s\n\n%s", message, string(errorPart)))
					} else {
						err = errors.Wrap(mysqlErr, "")
					}

					return driver.rollBackOnError(err, tx)
				}
			}
		}
	}

	return tx.Commit()
}

func (driver *Driver) Version() (uint64, error) {
	var version uint64
	err := driver.db.QueryRow("SELECT version FROM " + tableName + " ORDER BY version DESC").Scan(&version)
	switch {
	case err == sql.ErrNoRows:
		return 0, nil
	case err != nil:
		return 0, err
	default:
		return version, nil
	}
}

func (driver *Driver) Baseline(version uint64) error {
	tx, err := driver.db.Begin()

	if err != nil {
		return err
	}

	if _, err := tx.Exec("INSERT INTO "+tableName+" (version) VALUES (?)", version); err != nil {
		return driver.rollBackOnError(err, tx)
	}

	if err := tx.Commit(); err != nil {
		return driver.rollBackOnError(err, tx)
	}

	return nil
}

func init() {
	driver.RegisterDriver("mysql", &Driver{})
}
