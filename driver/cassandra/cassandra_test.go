package cassandra

import (
	"context"
	"net/url"
	"os"
	"testing"
	"time"

	"github.com/gocql/gocql"
	testdriver "gitlab.com/pieter.lazzaro/migrate/driver/test"
	"gitlab.com/pieter.lazzaro/migrate/file"
	"gitlab.com/pieter.lazzaro/migrate/migrate/direction"
)

func TestMigrate(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	var session *gocql.Session

	host := os.Getenv("CASSANDRA_PORT_9042_TCP_ADDR")
	port := os.Getenv("CASSANDRA_PORT_9042_TCP_PORT")
	driverUrl := "cassandra://" + host + ":" + port + "/system"

	// prepare a clean test database
	u, err := url.Parse(driverUrl)
	if err != nil {
		t.Fatal(err)
	}

	cluster := gocql.NewCluster(u.Host)
	cluster.Keyspace = u.Path[1:len(u.Path)]
	cluster.Consistency = gocql.All
	cluster.Timeout = 1 * time.Minute

	session, err = cluster.CreateSession()

	if err != nil {
		t.Fatal(err)
	}

	if err := session.Query(`CREATE KEYSPACE IF NOT EXISTS migrate WITH REPLICATION = {'class': 'SimpleStrategy', 'replication_factor': 1};`).Exec(); err != nil {
		t.Fatal(err)
	}
	cluster.Keyspace = "migrate"
	session, err = cluster.CreateSession()
	driverUrl = "cassandra://" + host + ":" + port + "/migrate"

	d := &Driver{}
	if err := d.Initialize(driverUrl); err != nil {
		t.Fatal(err)
	}

	files := []file.File{
		{
			Path:      "/foobar",
			FileName:  "001_foobar.up.sql",
			Version:   1,
			Name:      "foobar",
			Direction: direction.Up,
			Content: []byte(`
                CREATE TABLE yolo (
                    id varint primary key,
                    msg text
                );

				CREATE INDEX ON yolo (msg);
            `),
		},
		{
			Path:      "/foobar",
			FileName:  "002_foobar.down.sql",
			Version:   1,
			Name:      "foobar",
			Direction: direction.Down,
			Content: []byte(`
                DROP TABLE yolo;
            `),
		},
		{
			Path:      "/foobar",
			FileName:  "002_foobar.up.sql",
			Version:   2,
			Name:      "foobar",
			Direction: direction.Up,
			Content: []byte(`
                CREATE TABLE error (
                    id THIS WILL CAUSE AN ERROR
                )
            `),
		},
	}

	if err := d.Migrate(context.Background(), files[0], testdriver.NewLogger(t)); err != nil {
		t.Fatal(err)
	}

	if err := d.Migrate(context.Background(), files[1], testdriver.NewLogger(t)); err != nil {
		t.Fatal(err)
	}

	if err := d.Migrate(context.Background(), files[2], testdriver.NewLogger(t)); err == nil {
		t.Fatal("should have got an error")
	}
	if err := d.Close(); err != nil {
		t.Fatal(err)
	}
}

func TestInitializeReturnsErrorsForBadUrls(t *testing.T) {
	var session *gocql.Session

	host := os.Getenv("CASSANDRA_PORT_9042_TCP_ADDR")
	port := os.Getenv("CASSANDRA_PORT_9042_TCP_PORT")

	cluster := gocql.NewCluster(host)
	cluster.Consistency = gocql.All
	cluster.Timeout = 1 * time.Minute

	session, err := cluster.CreateSession()
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()
	if err := session.Query(`CREATE KEYSPACE IF NOT EXISTS migrate WITH REPLICATION = {'class': 'SimpleStrategy', 'replication_factor': 1};`).Exec(); err != nil {
		t.Fatal(err)
	}

	d := &Driver{}
	invalidURL := "sdf://asdf://as?df?a"
	if err := d.Initialize(invalidURL); err == nil {
		t.Errorf("expected an error to be returned if url could not be parsed")
	}

	noKeyspace := "cassandra://" + host + ":" + port
	if err := d.Initialize(noKeyspace); err == nil {
		t.Errorf("expected an error to be returned if no keyspace provided")
	}
}
