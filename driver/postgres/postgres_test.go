package postgres

import (
	"context"
	"database/sql"
	"os"
	"testing"

	testdriver "gitlab.com/pieter.lazzaro/migrate/driver/test"
	"gitlab.com/pieter.lazzaro/migrate/file"
	"gitlab.com/pieter.lazzaro/migrate/migrate/direction"
)

// TestMigrate runs some additional tests on Migrate().
func TestMigrate(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	host := os.Getenv("POSTGRES_PORT_5432_TCP_ADDR")
	port := os.Getenv("POSTGRES_PORT_5432_TCP_PORT")
	driverUrl := "postgres://postgres@" + host + ":" + port + "/template1?sslmode=disable"

	// prepare clean database
	connection, err := sql.Open("postgres", driverUrl)
	if err != nil {
		t.Fatal(err)
	}
	if _, err := connection.Exec(`
				DROP TABLE IF EXISTS yolo;
				DROP TABLE IF EXISTS ` + tableName + `;`); err != nil {
		t.Fatal(err)
	}

	d := &Driver{}
	if err := d.Initialize(driverUrl); err != nil {
		t.Fatal(err)
	}

	files := []file.File{
		{
			Path:      "/foobar",
			FileName:  "001_foobar.up.sql",
			Version:   1,
			Name:      "foobar",
			Direction: direction.Up,
			Content: []byte(`
				CREATE TABLE yolo (
					id serial not null primary key
				);
			`),
		},
		{
			Path:      "/foobar",
			FileName:  "002_foobar.down.sql",
			Version:   1,
			Name:      "foobar",
			Direction: direction.Down,
			Content: []byte(`
				DROP TABLE yolo;
			`),
		},
		{
			Path:      "/foobar",
			FileName:  "002_foobar.up.sql",
			Version:   2,
			Name:      "foobar",
			Direction: direction.Up,
			Content: []byte(`
				CREATE TABLE error (
					id THIS WILL CAUSE AN ERROR
				)
			`),
		},
	}

	if err := d.Migrate(context.Background(), files[0], testdriver.NewLogger(t)); err != nil {
		t.Fatal(err)
	}

	if err := d.Migrate(context.Background(), files[1], testdriver.NewLogger(t)); err != nil {
		t.Fatal(err)
	}

	if err := d.Migrate(context.Background(), files[2], testdriver.NewLogger(t)); err == nil {
		t.Fatal("should have got an error")
	}

	if err := d.Close(); err != nil {
		t.Fatal(err)
	}
}
