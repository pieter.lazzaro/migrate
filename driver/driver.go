// Package driver holds the driver interface.
package driver

import (
	"context"
	"fmt"
	// alias to allow `url string` func signature in New
	"strings"

	"gitlab.com/pieter.lazzaro/migrate/file"
)

type Logger interface {
	Print(args ...interface{})
	Printf(format string, args ...interface{})
	Println(args ...interface{})
}

// Driver is the interface type that needs to implemented by all drivers.
type Driver interface {

	// Initialize is the first function to be called.
	// Check the url string and open and verify any connection
	// that has to be made.
	Initialize(url string) error

	// Close is the last function to be called.
	// Close any open connection here.
	Close() error

	// FilenameExtension returns the extension of the migration files.
	// The returned string must not begin with a dot.
	FilenameExtension() string

	// Migrate is the heart of the driver.
	// It will receive a file which the driver should apply
	// to its backend or whatever.
	Migrate(ctx context.Context, file file.File, log Logger) error

	// Baseline will force the database to version. This is useful
	// for databases that already exist.
	Baseline(version uint64) error

	// Version returns the current migration version.
	Version() (uint64, error)
}

// New returns Driver and calls Initialize on it
func New(url string) (Driver, error) {
	parts := strings.SplitN(url, "://", 2)

	if len(parts) != 2 {
		return nil, fmt.Errorf("invalid url")
	}

	d := GetDriver(parts[0])

	if d == nil {
		return nil, fmt.Errorf("Driver '%s' not found.", parts[0])
	}
	verifyFilenameExtension(parts[0], d)
	if err := d.Initialize(url); err != nil {
		return nil, err
	}

	return d, nil
}

// verifyFilenameExtension panics if the driver's filename extension
// is not correct or empty.
func verifyFilenameExtension(driverName string, d Driver) {
	f := d.FilenameExtension()
	if f == "" {
		panic(fmt.Sprintf("%s.FilenameExtension() returns empty string.", driverName))
	}
	if f[0:1] == "." {
		panic(fmt.Sprintf("%s.FilenameExtension() returned string must not start with a dot.", driverName))
	}
}
