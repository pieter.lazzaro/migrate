package bash

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	testdriver "gitlab.com/pieter.lazzaro/migrate/driver/test"
	"gitlab.com/pieter.lazzaro/migrate/file"
	"gitlab.com/pieter.lazzaro/migrate/migrate/direction"
)

func TestDriverVersion(t *testing.T) {

	tests := []struct {
		fileContents    []byte
		expectedVersion uint64
	}{
		{[]byte("0"), uint64(0)},
		{[]byte("0\n1"), uint64(1)},
		{[]byte("0\n1\n3"), uint64(3)},
	}

	for _, tc := range tests {
		t.Run(fmt.Sprintf("%d: '%s'", tc.expectedVersion, tc.fileContents), func(t *testing.T) {
			tmpdir, err := ioutil.TempDir("/tmp", "bash-test")
			if err != nil {
				t.Fatal(err)
			}
			defer os.RemoveAll(tmpdir)

			if err := ioutil.WriteFile(tmpdir+"/version", tc.fileContents, 0644); err != nil {
				t.Fatal(err)
			}

			driver := Driver{}

			if err := driver.Initialize("bash://" + tmpdir + "/version"); err != nil {
				t.Fatal(err)
			}

			testdriver.AssertVersion(t, &driver, tc.expectedVersion)
		})
	}

}

func TestDriverMigrateRunScript(t *testing.T) {

	tmpdir, err := ioutil.TempDir("/tmp", "bash-test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tmpdir)

	f := file.File{
		Direction: direction.Up,
		Version:   uint64(1),
		FileName:  "test",
		Name:      "test",
		Content: []byte(fmt.Sprintf(`#!/bin/bash
touch %s
`, tmpdir+"/test")),
	}

	driver := Driver{}

	if err := driver.Initialize("bash://" + tmpdir + "/version"); err != nil {
		t.Fatal(err)
	}

	if err := driver.Migrate(context.Background(), f, testdriver.NewLogger(t)); err != nil {
		t.Fatal(err)
	}

	if _, err := os.Stat(tmpdir + "/test"); err != nil {
		t.Fatal("Should have created file.")
	}
}

func TestDriverMigrateUpUpdatesTheVersion(t *testing.T) {

	tmpdir, err := ioutil.TempDir("/tmp", "bash-test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tmpdir)

	f := file.File{
		Direction: direction.Up,
		Version:   uint64(1),
		FileName:  "test",
		Name:      "test",
		Content: []byte(fmt.Sprintf(`#!/bin/bash
touch %s
`, tmpdir+"/test")),
	}

	driver := Driver{}

	if err := driver.Initialize("bash://" + tmpdir + "/version"); err != nil {
		t.Fatal(err)
	}

	if err := driver.Migrate(context.Background(), f, testdriver.NewLogger(t)); err != nil {
		t.Fatal(err)
	}

	testdriver.AssertVersion(t, &driver, 1)
}

func TestDriverMigrateDownUpdatesTheVersion(t *testing.T) {

	tmpdir, err := ioutil.TempDir("/tmp", "bash-test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tmpdir)

	if err := ioutil.WriteFile(tmpdir+"/version", []byte("0\n1\n"), 0644); err != nil {
		t.Fatal(err)
	}

	f := file.File{
		Direction: direction.Down,
		Version:   uint64(1),
		FileName:  tmpdir + "/test",
		Name:      "test",
		Content: []byte(fmt.Sprintf(`#!/bin/bash
touch %s
`, tmpdir+"/test")),
	}

	driver := Driver{}

	if err := driver.Initialize("bash://" + tmpdir + "/version"); err != nil {
		t.Fatal(err)
	}

	if err := driver.Migrate(context.Background(), f, testdriver.NewLogger(t)); err != nil {
		t.Fatal(err)
	}

	testdriver.AssertVersion(t, &driver, 0)
}

func TestDriverWritesVersionInformationOnClose(t *testing.T) {

	tmpdir, err := ioutil.TempDir("/tmp", "bash-test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tmpdir)

	f := file.File{
		Direction: direction.Up,
		Version:   uint64(1),
		FileName:  tmpdir + "/test",
		Name:      "test",
		Content: []byte(fmt.Sprintf(`#!/bin/bash
touch %s
`, tmpdir+"/test")),
	}

	driver := Driver{}

	if err := driver.Initialize("bash://" + tmpdir + "/version"); err != nil {
		t.Fatal(err)
	}

	if err := driver.Migrate(context.Background(), f, testdriver.NewLogger(t)); err != nil {
		t.Fatal(err)
	}

	if err := driver.Close(); err != nil {
		t.Fatal(err)
	}

	versionData, err := ioutil.ReadFile(tmpdir + "/version")

	if err != nil {
		t.Fatal(err)
	}

	if string(versionData) != "0\n1\n" {
		t.Fatalf("Version file not written correctly. %s", versionData)
	}
}
