// Package bash implements the Driver interface.
package bash

import (
	"bytes"
	"context"
	"fmt"
	"os/exec"
	"path"

	"strings"

	"os"

	"io/ioutil"

	"bufio"

	"strconv"

	"gitlab.com/pieter.lazzaro/migrate/driver"
	"gitlab.com/pieter.lazzaro/migrate/file"
	"gitlab.com/pieter.lazzaro/migrate/migrate/direction"
)

type versionInfo uint64

type Driver struct {
	filename string
	versions []versionInfo
}

func (driver *Driver) readVersionFile() error {
	driver.versions = make([]versionInfo, 0)

	file, err := os.Open(driver.filename)

	if err != nil {
		return err
	}

	versionScanner := bufio.NewScanner(file)

	for versionScanner.Scan() {
		v, err := strconv.ParseUint(versionScanner.Text(), 10, 64)

		if err != nil {
			return err
		}

		driver.versions = append(driver.versions, versionInfo(v))
	}

	return nil
}

func (driver *Driver) Initialize(url string) error {
	driver.filename = path.Clean(strings.TrimPrefix(url, "bash:/"))

	if _, err := os.Stat(driver.filename); os.IsNotExist(err) {
		return ioutil.WriteFile(driver.filename, []byte("0"), 0644)
	}

	return driver.readVersionFile()
}

func (driver *Driver) writeVersionFile() error {
	versions := make([]string, len(driver.versions))
	for i := range driver.versions {
		versions[i] = fmt.Sprintf("%d", driver.versions[i])
	}

	if versions[0] != "0" {
		versions = append([]string{"0"}, versions...)
	}

	return ioutil.WriteFile(driver.filename, []byte(strings.Join(versions, "\n")+"\n"), 0600)
}

func (driver *Driver) Close() error {

	return driver.writeVersionFile()
}

func (driver *Driver) FilenameExtension() string {
	return "sh"
}

func (driver *Driver) Migrate(ctx context.Context, f file.File, log driver.Logger) error {
	filename := path.Join(os.TempDir(), f.Name)

	if err := f.ReadContent(); err != nil {
		return err
	}

	if err := ioutil.WriteFile(filename, f.Content, 0700); err != nil {
		return err
	}

	cmd := exec.Command(filename)

	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		return err
	}

	if f.Direction == direction.Up {
		return driver.updateVersion(f.Version)
	}

	return driver.removeVersion(f.Version)

}

func (driver *Driver) removeVersion(version uint64) error {

	for i, v := range driver.versions {
		if uint64(v) == version {
			driver.versions = append(driver.versions[:i], driver.versions[i+1:]...)
		}
	}
	return driver.writeVersionFile()
}

func (driver *Driver) updateVersion(version uint64) error {

	driver.versions = append(driver.versions, versionInfo(version))

	return driver.writeVersionFile()
}

func (driver *Driver) Version() (uint64, error) {
	version := uint64(0)

	for _, v := range driver.versions {

		if uint64(v) > version {
			version = uint64(v)
		}
	}

	return version, nil
}

func (driver *Driver) Baseline(version uint64) error {

	return driver.updateVersion(version)
}

func init() {
	driver.RegisterDriver("bash", &Driver{})
}
