package sqlite3

import (
	"context"
	"database/sql"
	"testing"

	testdriver "gitlab.com/pieter.lazzaro/migrate/driver/test"
	"gitlab.com/pieter.lazzaro/migrate/file"
	"gitlab.com/pieter.lazzaro/migrate/migrate/direction"
)

// TestMigrate runs some additional tests on Migrate()
// Basic testing is already done in migrate/migrate_test.go
func TestMigrate(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	driverFile := ":memory:"
	driverUrl := "sqlite3://" + driverFile

	// prepare clean database
	connection, err := sql.Open("sqlite3", driverFile)
	if err != nil {
		t.Fatal(err)
	}
	if _, err := connection.Exec(`
							DROP TABLE IF EXISTS yolo;
							DROP TABLE IF EXISTS ` + tableName + `;`); err != nil {
		t.Fatal(err)
	}

	d := &Driver{}
	if err := d.Initialize(driverUrl); err != nil {
		t.Fatal(err)
	}

	files := []file.File{
		{
			Path:      "/foobar",
			FileName:  "001_foobar.up.sql",
			Version:   1,
			Name:      "foobar",
			Direction: direction.Up,
			Content: []byte(`
				CREATE TABLE yolo (
					id INTEGER PRIMARY KEY AUTOINCREMENT
				);
			`),
		},
		{
			Path:      "/foobar",
			FileName:  "002_foobar.down.sql",
			Version:   1,
			Name:      "foobar",
			Direction: direction.Down,
			Content: []byte(`
				DROP TABLE yolo;
			`),
		},
		{
			Path:      "/foobar",
			FileName:  "002_foobar.up.sql",
			Version:   1,
			Name:      "foobar",
			Direction: direction.Down,
			Content: []byte(`
				CREATE TABLE error (
					THIS; WILL CAUSE; AN ERROR;
				)
			`),
		},
	}

	if err := d.Migrate(context.Background(), files[0], testdriver.NewLogger(t)); err != nil {
		t.Fatal(err)
	}

	if err := d.Migrate(context.Background(), files[1], testdriver.NewLogger(t)); err != nil {
		t.Fatal(err)
	}

	if err := d.Migrate(context.Background(), files[2], testdriver.NewLogger(t)); err == nil {
		t.Fatal("should have got an error")
	}

	if err := d.Close(); err != nil {
		t.Fatal(err)
	}
}
